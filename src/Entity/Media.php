<?php

namespace App\Entity;

use App\Repository\MediaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=MediaRepository::class)
 * @ORM\Table(name="media")
 * @Vich\Uploadable
 */
class Media implements \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"media"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"media"})
     */
    private $name;
    
    /**
    * @Vich\UploadableField(mapping="media", fileNameProperty="filename")
     * @Groups({"media"})
    */
    private $file;

    /**
    * @ORM\Column(type="string", nullable=false)
     * @Groups({"media"})
    */
    private $filename;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"media"})
     */
    private $ext;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"media"})
     */
    private $mime;

    /**
    * @ORM\Column(type="datetime", nullable=false)
     * @Groups({"media"})
    */
    private $datetime;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     * @Groups({"media"})
     */
    private $categories = [];

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="profileImage")
     */
    private $profileImageUser;

    /**
     * @ORM\OneToMany(targetEntity=Config::class, mappedBy="appLogo")
     */
    private $appLogoConfig;

    /**
     * @ORM\OneToMany(targetEntity=Config::class, mappedBy="appHeaderLogo")
     */
    private $appHeaderLogoConfig;

    /**
     * @ORM\OneToMany(targetEntity=Config::class, mappedBy="appFavicon")
     */
    private $appFaviconConfig;

    public function __construct()
    {
        $this->profileImageUser = new ArrayCollection();
        $this->appLogoConfig = new ArrayCollection();
        $this->appHeaderLogoConfig = new ArrayCollection();
        $this->appFaviconConfig = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return $this->filename;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
    
    public function setFile(?File $file = null): void
    {
        $this->file = $file;
        $this->setDatetime(new \DateTimeImmutable());
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFilename(?string $filename): void
    {
        $this->filename = $filename;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function getExt(): ?string
    {
        return $this->ext;
    }

    public function setExt(string $ext): self
    {
        $this->ext = $ext;

        return $this;
    }

    public function getMime(): ?string
    {
        return $this->mime;
    }

    public function setMime(string $mime): self
    {
        $this->mime = $mime;

        return $this;
    }

    public function setDatetime(?\DateTimeInterface $datetime): void
    {
        $this->datetime = $datetime;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
        ) = unserialize($serialized);
    }

    public function getCategories(): ?array
    {
        return $this->categories;
    }

    public function setCategories(?array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function addCategory($category): self
    {
        if (!in_array($category, $this->categories)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory($category): self
    {
        if (in_array($category, $this->categories)) {
            $key = array_search($category, $this->categories);
            unset($this->categories[$key]);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getProfileImageUser(): Collection
    {
        return $this->profileImageUser;
    }

    public function addProfileImageUser(User $profileImageUser): self
    {
        if (!$this->profileImageUser->contains($profileImageUser)) {
            $this->profileImageUser[] = $profileImageUser;
            $profileImageUser->setProfileImage($this);
        }

        return $this;
    }

    public function removeProfileImageUser(User $profileImageUser): self
    {
        if ($this->profileImageUser->removeElement($profileImageUser)) {
            // set the owning side to null (unless already changed)
            if ($profileImageUser->getProfileImage() === $this) {
                $profileImageUser->setProfileImage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Config[]
     */
    public function getAppLogoConfig(): Collection
    {
        return $this->appLogoConfig;
    }

    public function addAppLogoConfig(Config $appLogoConfig): self
    {
        if (!$this->appLogoConfig->contains($appLogoConfig)) {
            $this->appLogoConfig[] = $appLogoConfig;
            $appLogoConfig->setAppLogo($this);
        }

        return $this;
    }

    public function removeAppLogoConfig(Config $appLogoConfig): self
    {
        if ($this->appLogoConfig->removeElement($appLogoConfig)) {
            // set the owning side to null (unless already changed)
            if ($appLogoConfig->getAppLogo() === $this) {
                $appLogoConfig->setAppLogo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Config[]
     */
    public function getAppHeaderLogoConfig(): Collection
    {
        return $this->appHeaderLogoConfig;
    }

    public function addAppHeaderLogoConfig(Config $appHeaderLogoConfig): self
    {
        if (!$this->appHeaderLogoConfig->contains($appHeaderLogoConfig)) {
            $this->appHeaderLogoConfig[] = $appHeaderLogoConfig;
            $appHeaderLogoConfig->setAppHeaderLogo($this);
        }

        return $this;
    }

    public function removeAppHeaderLogoConfig(Config $appHeaderLogoConfig): self
    {
        if ($this->appHeaderLogoConfig->removeElement($appHeaderLogoConfig)) {
            // set the owning side to null (unless already changed)
            if ($appHeaderLogoConfig->getAppHeaderLogo() === $this) {
                $appHeaderLogoConfig->setAppHeaderLogo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Config[]
     */
    public function getAppFaviconConfig(): Collection
    {
        return $this->appFaviconConfig;
    }

    public function addAppFaviconConfig(Config $appFaviconConfig): self
    {
        if (!$this->appFaviconConfig->contains($appFaviconConfig)) {
            $this->appFaviconConfig[] = $appFaviconConfig;
            $appFaviconConfig->setAppFavicon($this);
        }

        return $this;
    }

    public function removeAppFaviconConfig(Config $appFaviconConfig): self
    {
        if ($this->appFaviconConfig->removeElement($appFaviconConfig)) {
            // set the owning side to null (unless already changed)
            if ($appFaviconConfig->getAppFavicon() === $this) {
                $appFaviconConfig->setAppFavicon(null);
            }
        }

        return $this;
    }
}
