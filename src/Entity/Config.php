<?php

namespace App\Entity;

use App\Repository\ConfigRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConfigRepository::class)
 * @ORM\Table(name="config")
 */
class Config
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $appName;

    /**
     * @ORM\ManyToOne(targetEntity=Media::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="appLogo", referencedColumnName="id", onDelete="SET NULL")
     */
    private $appLogo;

    /**
     * @ORM\ManyToOne(targetEntity=Media::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="appHeaderLogo", referencedColumnName="id", onDelete="SET NULL")
     */
    private $appHeaderLogo;

    /**
     * @ORM\ManyToOne(targetEntity=Media::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="appFavicon", referencedColumnName="id", onDelete="SET NULL")
     */
    private $appFavicon;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $floatingAlerts;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enableUsername;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enablePublicLogin;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enableRegister;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enablePublicHeader;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enablePublicFooter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $senderEmail;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $timezone;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $entityActionsAsDropdown;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $paginatorPageSize;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAppName(): ?string
    {
        return $this->appName;
    }

    public function setAppName(?string $appName): self
    {
        $this->appName = $appName;

        return $this;
    }

    public function getAppLogo(): ?Media
    {
        return $this->appLogo;
    }

    public function setAppLogo(?Media $appLogo): self
    {
        if ($this->appLogo && count($this->appLogo->getAppLogoConfig()) == 1) {
            $this->appLogo->removeCategory("appLogoConfig");
        }
        if ($appLogo) {
            $appLogo->addCategory("appLogoConfig");
        }
        $this->appLogo = $appLogo;

        return $this;
    }

    public function getAppHeaderLogo(): ?Media
    {
        return $this->appHeaderLogo;
    }

    public function setAppHeaderLogo(?Media $appHeaderLogo): self
    {
        if ($this->appHeaderLogo && count($this->appHeaderLogo->getAppHeaderLogoConfig()) == 1) {
            $this->appHeaderLogo->removeCategory("appHeaderLogoConfig");
        }
        if ($appHeaderLogo) {
            $appHeaderLogo->addCategory("appHeaderLogoConfig");
        }
        $this->appHeaderLogo = $appHeaderLogo;

        return $this;
    }

    public function getAppFavicon(): ?Media
    {
        return $this->appFavicon;
    }

    public function setAppFavicon(?Media $appFavicon): self
    {
        if ($this->appFavicon && count($this->appFavicon->getAppFaviconConfig()) == 1) {
            $this->appFavicon->removeCategory("appFaviconConfig");
        }
        if ($appFavicon) {
            $appFavicon->addCategory("appFaviconConfig");
        }
        $this->appFavicon = $appFavicon;

        return $this;
    }

    public function getFloatingAlerts(): ?bool
    {
        return $this->floatingAlerts;
    }

    public function setFloatingAlerts(bool $floatingAlerts): self
    {
        $this->floatingAlerts = $floatingAlerts;

        return $this;
    }

    public function getEnableUsername(): ?bool
    {
        return $this->enableUsername;
    }

    public function setEnableUsername(?bool $enableUsername): self
    {
        $this->enableUsername = $enableUsername;

        return $this;
    }

    public function getEnablePublicLogin(): ?bool
    {
        return $this->enablePublicLogin;
    }

    public function setEnablePublicLogin(bool $enablePublicLogin): self
    {
        $this->enablePublicLogin = $enablePublicLogin;

        return $this;
    }

    public function getEnableRegister(): ?bool
    {
        return $this->enableRegister;
    }

    public function setEnableRegister(bool $enableRegister): self
    {
        $this->enableRegister = $enableRegister;

        return $this;
    }

    public function getEnablePublicHeader(): ?bool
    {
        return $this->enablePublicHeader;
    }

    public function setEnablePublicHeader(bool $enablePublicHeader): self
    {
        $this->enablePublicHeader = $enablePublicHeader;

        return $this;
    }

    public function getEnablePublicFooter(): ?bool
    {
        return $this->enablePublicFooter;
    }

    public function setEnablePublicFooter(bool $enablePublicFooter): self
    {
        $this->enablePublicFooter = $enablePublicFooter;

        return $this;
    }

    public function getSenderEmail(): ?string
    {
        return $this->senderEmail;
    }

    public function setSenderEmail(?string $senderEmail): self
    {
        $this->senderEmail = $senderEmail;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(?string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getEntityActionsAsDropdown(): ?bool
    {
        return $this->entityActionsAsDropdown;
    }

    public function setEntityActionsAsDropdown(?bool $entityActionsAsDropdown): self
    {
        $this->entityActionsAsDropdown = $entityActionsAsDropdown;

        return $this;
    }

    public function getPaginatorPageSize(): ?int
    {
        return $this->paginatorPageSize;
    }

    public function setPaginatorPageSize(?int $paginatorPageSize): self
    {
        $this->paginatorPageSize = $paginatorPageSize;

        return $this;
    }
}
