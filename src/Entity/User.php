<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="user")
 * @UniqueEntity(fields={"email"})
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user"})
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @Groups({"user"})
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $verified = false;

    /**
     * @ORM\ManyToOne(targetEntity=Media::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="profileImage", referencedColumnName="id", onDelete="SET NULL")
     */
    private $profileImage;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $locale;

    /**
     * @ORM\ManyToMany(targetEntity=Role::class, inversedBy="users")
     */
    private $entityRoles;

    /**
     * @var array
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity=Tab::class, mappedBy="user", orphanRemoval=true)
     * @ORM\OrderBy({"position"="ASC", "name"="ASC"})
     */
    private $tabs;

    public function __construct()
    {
        $this->entityRoles = new ArrayCollection();
        $this->tabs = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return $this->getUsername();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFullname(): string
    {
        return (string) $this->name . ' ' . $this->lastname;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }
    
    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }
    
    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getVerified(): bool
    {
        return $this->verified;
    }

    public function setVerified(bool $verified): self
    {
        $this->verified = $verified;

        return $this;
    }

    public function getProfileImage(): ?Media
    {
        return $this->profileImage;
    }

    public function setProfileImage(?Media $profileImage): self
    {
        if ($this->profileImage && count($this->profileImage->getProfileImageUser()) == 1) {
            $this->profileImage->removeCategory("profileImageUser");
        }
        if ($profileImage) {
            $profileImage->addCategory("profileImageUser");
        }
        $this->profileImage = $profileImage;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get Entities of Roles
     * @return Collection|Role[]
     */
    public function getEntityRoles(): Collection
    {
        return $this->entityRoles;
    }

    public function addEntityRole(Role $entityRole): self
    {
        if (!$this->entityRoles->contains($entityRole)) {
            $this->entityRoles[] = $entityRole;
        }

        return $this;
    }

    public function removeEntityRole(Role $entityRole): self
    {
        $this->entityRoles->removeElement($entityRole);

        return $this;
    }

    /**
     * Get plained Roles
     * @return array
     */
    public function getRoles(): ?array
    {
        $this->roles = array();
        foreach ($this->entityRoles as $entityRole) {
            $this->roles[] = $entityRole->getId();
        }
        return $this->roles;
    }
    
    public function hasRole(string $_role): bool
    {
        $hasRole = false;
        foreach ($this->entityRoles as $entityRole) {
            if ($entityRole->getId() == $_role) {
                $hasRole = true;
                break;
            }
        }
        return $hasRole;
    }
    
    public function isRole(string $_role): bool
    {
        return count($this->entityRoles) == 1 && $this->hasRole($_role);
    }
    
    public function hasPermission(string $_permission): bool
    {
        //Clear ROLE_ADMIN permissions if user have more than one role
        $myRoles = array();
        foreach ($this->entityRoles as $entityRole) {
            if (count($this->entityRoles) == 1 || count($this->entityRoles) > 1 && $entityRole->getId() != "ROLE_ADMIN") {
                $myRoles[] = $entityRole;
            }
        }

        $hasPermission = false;
        foreach ($myRoles as $entityRole) {
            if ($entityRole->getPermission($_permission)) {
                $hasPermission = true;
                break;
            }
        }
        return $hasPermission;
    }

    /**
     * @return Collection|Tab[]
     */
    public function getTabs(): Collection
    {
        return $this->tabs;
    }

    public function addTab(Tab $tab): self
    {
        if (!$this->tabs->contains($tab)) {
            $this->tabs[] = $tab;
            $tab->setUser($this);
        }

        return $this;
    }

    public function removeTab(Tab $tab): self
    {
        if ($this->tabs->removeElement($tab)) {
            // set the owning side to null (unless already changed)
            if ($tab->getUser() === $this) {
                $tab->setUser(null);
            }
        }

        return $this;
    }
}
