<?php

namespace App\Entity;

use App\Repository\RoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=RoleRepository::class)
 * @ORM\Table(name="role")
 * @UniqueEntity(fields={"id", "name"})
 */
class Role
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="entityRoles")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_showMedia = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editMedia = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_deleteMedia = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_showUser = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_createUser = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editUser = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_deleteUser = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_showUserAdmin = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_createUserAdmin = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editUserAdmin = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_deleteUserAdmin = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_createUserSuperadmin = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editUserSuperadmin = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_deleteUserSuperadmin = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_showRole = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_createRole = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editRole = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_deleteRole = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_showRoleSuperadmin = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editRoleSuperadmin = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_showRoleUser = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editRoleUser = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editRoleSelf = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_deleteRoleSelf = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_showConfig = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editConfig = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_enableLoginAndRegister = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_showTab = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_createTab = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editTab = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_deleteTab = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_showLink = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_createLink = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_editLink = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permission_deleteLink = false;

    public function __toString()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addEntityRole($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeEntityRole($this);
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        if (!$this->id) {
            $this->setId("ROLE_" . $this->cleanString($name));
        }

        return $this;
    }

    private function cleanString($text) {
        // Remove non letter or digits
        $text = preg_replace('~[^\pL\d]+~u', '', $text);
        // Transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // Remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        // Uppercase
        $text = strtoupper($text);
        return $text;
    }

    public function getPermission(string $_permission): ?bool
    {
        $permissionName = 'permission_' . lcfirst($_permission);
        $value = property_exists($this, $permissionName) ? $this->{$permissionName} : false;
        return $value;
    }

    public function getPermissionShowMedia(): ?bool
    {
        return $this->permission_showMedia;
    }

    public function setPermissionShowMedia(bool $permission_showMedia): self
    {
        $this->permission_showMedia = $permission_showMedia;

        return $this;
    }

    public function getPermissionEditMedia(): ?bool
    {
        return $this->permission_editMedia;
    }

    public function setPermissionEditMedia(bool $permission_editMedia): self
    {
        $this->permission_editMedia = $permission_editMedia;

        return $this;
    }

    public function getPermissionDeleteMedia(): ?bool
    {
        return $this->permission_deleteMedia;
    }

    public function setPermissionDeleteMedia(bool $permission_deleteMedia): self
    {
        $this->permission_deleteMedia = $permission_deleteMedia;

        return $this;
    }

    public function getPermissionShowUser(): ?bool
    {
        return $this->permission_showUser;
    }

    public function setPermissionShowUser(bool $permission_showUser): self
    {
        $this->permission_showUser = $permission_showUser;

        return $this;
    }

    public function getPermissionCreateUser(): ?bool
    {
        return $this->permission_createUser;
    }

    public function setPermissionCreateUser(bool $permission_createUser): self
    {
        $this->permission_createUser = $permission_createUser;

        return $this;
    }

    public function getPermissionEditUser(): ?bool
    {
        return $this->permission_editUser;
    }

    public function setPermissionEditUser(bool $permission_editUser): self
    {
        $this->permission_editUser = $permission_editUser;

        return $this;
    }

    public function getPermissionDeleteUser(): ?bool
    {
        return $this->permission_deleteUser;
    }

    public function setPermissionDeleteUser(bool $permission_deleteUser): self
    {
        $this->permission_deleteUser = $permission_deleteUser;

        return $this;
    }

    public function getPermissionShowUserAdmin(): ?bool
    {
        return $this->permission_showUserAdmin;
    }

    public function setPermissionShowUserAdmin(bool $permission_showUserAdmin): self
    {
        $this->permission_showUserAdmin = $permission_showUserAdmin;

        return $this;
    }

    public function getPermissionCreateUserAdmin(): ?bool
    {
        return $this->permission_createUserAdmin;
    }

    public function setPermissionCreateUserAdmin(bool $permission_createUserAdmin): self
    {
        $this->permission_createUserAdmin = $permission_createUserAdmin;

        return $this;
    }

    public function getPermissionEditUserAdmin(): ?bool
    {
        return $this->permission_editUserAdmin;
    }

    public function setPermissionEditUserAdmin(bool $permission_editUserAdmin): self
    {
        $this->permission_editUserAdmin = $permission_editUserAdmin;

        return $this;
    }

    public function getPermissionDeleteUserAdmin(): ?bool
    {
        return $this->permission_deleteUserAdmin;
    }

    public function setPermissionDeleteUserAdmin(bool $permission_deleteUserAdmin): self
    {
        $this->permission_deleteUserAdmin = $permission_deleteUserAdmin;

        return $this;
    }

    public function getPermissionCreateUserSuperadmin(): ?bool
    {
        return $this->permission_createUserSuperadmin;
    }

    public function setPermissionCreateUserSuperadmin(bool $permission_createUserSuperadmin): self
    {
        $this->permission_createUserSuperadmin = $permission_createUserSuperadmin;

        return $this;
    }

    public function getPermissionEditUserSuperadmin(): ?bool
    {
        return $this->permission_editUserSuperadmin;
    }

    public function setPermissionEditUserSuperadmin(bool $permission_editUserSuperadmin): self
    {
        $this->permission_editUserSuperadmin = $permission_editUserSuperadmin;

        return $this;
    }

    public function getPermissionDeleteUserSuperadmin(): ?bool
    {
        return $this->permission_deleteUserSuperadmin;
    }

    public function setPermissionDeleteUserSuperadmin(bool $permission_deleteUserSuperadmin): self
    {
        $this->permission_deleteUserSuperadmin = $permission_deleteUserSuperadmin;

        return $this;
    }

    public function getPermissionShowRole(): ?bool
    {
        return $this->permission_showRole;
    }

    public function setPermissionShowRole(bool $permission_showRole): self
    {
        $this->permission_showRole = $permission_showRole;

        return $this;
    }

    public function getPermissionCreateRole(): ?bool
    {
        return $this->permission_createRole;
    }

    public function setPermissionCreateRole(bool $permission_createRole): self
    {
        $this->permission_createRole = $permission_createRole;

        return $this;
    }

    public function getPermissionEditRole(): ?bool
    {
        return $this->permission_editRole;
    }

    public function setPermissionEditRole(bool $permission_editRole): self
    {
        $this->permission_editRole = $permission_editRole;

        return $this;
    }

    public function getPermissionDeleteRole(): ?bool
    {
        return $this->permission_deleteRole;
    }

    public function setPermissionDeleteRole(bool $permission_deleteRole): self
    {
        $this->permission_deleteRole = $permission_deleteRole;

        return $this;
    }

    public function getPermissionShowRoleSuperadmin(): ?bool
    {
        return $this->permission_showRoleSuperadmin;
    }

    public function setPermissionShowRoleSuperadmin(bool $permission_showRoleSuperadmin): self
    {
        $this->permission_showRoleSuperadmin = $permission_showRoleSuperadmin;

        return $this;
    }

    public function getPermissionEditRoleSuperadmin(): ?bool
    {
        return $this->permission_editRoleSuperadmin;
    }

    public function setPermissionEditRoleSuperadmin(bool $permission_editRoleSuperadmin): self
    {
        $this->permission_editRoleSuperadmin = $permission_editRoleSuperadmin;

        return $this;
    }

    public function getPermissionShowRoleUser(): ?bool
    {
        return $this->permission_showRoleUser;
    }

    public function setPermissionShowRoleUser(bool $permission_showRoleUser): self
    {
        $this->permission_showRoleUser = $permission_showRoleUser;

        return $this;
    }

    public function getPermissionEditRoleUser(): ?bool
    {
        return $this->permission_editRoleUser;
    }

    public function setPermissionEditRoleUser(bool $permission_editRoleUser): self
    {
        $this->permission_editRoleUser = $permission_editRoleUser;

        return $this;
    }

    public function getPermissionEditRoleSelf(): ?bool
    {
        return $this->permission_editRoleSelf;
    }

    public function setPermissionEditRoleSelf(bool $permission_editRoleSelf): self
    {
        $this->permission_editRoleSelf = $permission_editRoleSelf;

        return $this;
    }

    public function getPermissionDeleteRoleSelf(): ?bool
    {
        return $this->permission_deleteRoleSelf;
    }

    public function setPermissionDeleteRoleSelf(bool $permission_deleteRoleSelf): self
    {
        $this->permission_deleteRoleSelf = $permission_deleteRoleSelf;

        return $this;
    }

    public function getPermissionShowConfig(): ?bool
    {
        return $this->permission_showConfig;
    }

    public function setPermissionShowConfig(bool $permission_showConfig): self
    {
        $this->permission_showConfig = $permission_showConfig;

        return $this;
    }

    public function getPermissionEditConfig(): ?bool
    {
        return $this->permission_editConfig;
    }

    public function setPermissionEditConfig(bool $permission_editConfig): self
    {
        $this->permission_editConfig = $permission_editConfig;

        return $this;
    }

    public function getPermissionEnableLoginAndRegister(): ?bool
    {
        return $this->permission_enableLoginAndRegister;
    }

    public function setPermissionEnableLoginAndRegister(bool $permission_enableLoginAndRegister): self
    {
        $this->permission_enableLoginAndRegister = $permission_enableLoginAndRegister;

        return $this;
    }

    public function getPermissionShowTab(): ?bool
    {
        return $this->permission_showTab;
    }

    public function setPermissionShowTab(bool $permission_showTab): self
    {
        $this->permission_showTab = $permission_showTab;

        return $this;
    }

    public function getPermissionCreateTab(): ?bool
    {
        return $this->permission_createTab;
    }

    public function setPermissionCreateTab(bool $permission_createTab): self
    {
        $this->permission_createTab = $permission_createTab;

        return $this;
    }

    public function getPermissionEditTab(): ?bool
    {
        return $this->permission_editTab;
    }

    public function setPermissionEditTab(bool $permission_editTab): self
    {
        $this->permission_editTab = $permission_editTab;

        return $this;
    }

    public function getPermissionDeleteTab(): ?bool
    {
        return $this->permission_deleteTab;
    }

    public function setPermissionDeleteTab(bool $permission_deleteTab): self
    {
        $this->permission_deleteTab = $permission_deleteTab;

        return $this;
    }

    public function getPermissionShowLink(): ?bool
    {
        return $this->permission_showLink;
    }

    public function setPermissionShowLink(bool $permission_showLink): self
    {
        $this->permission_showLink = $permission_showLink;

        return $this;
    }

    public function getPermissionCreateLink(): ?bool
    {
        return $this->permission_createLink;
    }

    public function setPermissionCreateLink(bool $permission_createLink): self
    {
        $this->permission_createLink = $permission_createLink;

        return $this;
    }

    public function getPermissionEditLink(): ?bool
    {
        return $this->permission_editLink;
    }

    public function setPermissionEditLink(bool $permission_editLink): self
    {
        $this->permission_editLink = $permission_editLink;

        return $this;
    }

    public function getPermissionDeleteLink(): ?bool
    {
        return $this->permission_deleteLink;
    }

    public function setPermissionDeleteLink(bool $permission_deleteLink): self
    {
        $this->permission_deleteLink = $permission_deleteLink;

        return $this;
    }
}
