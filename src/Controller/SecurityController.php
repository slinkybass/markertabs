<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

use App\Entity\User;
use App\Entity\Role;
use App\Form\RegistrationFormType;
use App\Form\RegistrationUsernameFormType;
use App\Form\ResetPasswordFormType;
use App\Form\ChangePasswordFormType;
use App\Security\EmailVerifier;
use App\Security\LoginFormAuthenticator;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Mailer\MailerInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

class SecurityController extends AbstractController
{
    use ResetPasswordControllerTrait;
    
    private $emailVerifier;
    private $resetPasswordHelper;
    private $translator;

    public function __construct(EmailVerifier $emailVerifier, ResetPasswordHelperInterface $resetPasswordHelper, TranslatorInterface $translator)
    {
        $this->emailVerifier = $emailVerifier;
        $this->resetPasswordHelper = $resetPasswordHelper;
        $this->translator = $translator;
    }

    /**
     * @Route("/login", name="public_login")
     */
    public function public_login(AuthenticationUtils $authenticationUtils): Response
    {
        // redirect if user is already logged
        if ($this->getUser()) {
            return $this->redirectToRoute('public_home');
        }

        // handle errors
        $error = $authenticationUtils->getLastAuthenticationError();
        if ($error) {
            $errorMsg = null;
            if ($error instanceof \Symfony\Component\Security\Core\Exception\BadCredentialsException) {
                $errorMsg = $this->translator->trans('messages.errors.login.badCredentials');
            } else {
                $errorMsg = $error->getMessageKey();
            }
            $this->addFlash('danger', $errorMsg);
        }

        return $this->render('public/login.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername()
        ]);
    }

    /**
     * @Route("/logout", name="public_logout")
     */
    public function public_logout()
    {
    }

    /**
     * @Route("/register", name="public_register")
     */
    public function public_register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator): Response
    {
        if (!$this->getParameter('enabled_public') || !$request->getSession()->get('config')->enableRegister) {
            return $this->redirectToRoute('public_login');
        }
        
        $user = new User();
        
        if ($request->getSession()->get('config')->enableUsername) {
            $form = $this->createForm(RegistrationUsernameFormType::class, $user);
        } else {
            $form = $this->createForm(RegistrationFormType::class, $user);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $roleUser = $em->getRepository(Role::class)->find("ROLE_USER");
            if ($roleUser) {
                $user->addEntityRole($roleUser);
            }

            if (!$request->getSession()->get('config')->enableUsername) {
                $user->setUsername($user->getEmail());
            }

            $em->persist($user);
            $em->flush();

            // generate a signed url and email it to the user
            try {
                $this->emailVerifier->sendEmailConfirmation('public_verifyEmail', $user,
                    (new TemplatedEmail())
                        ->from(new Address($request->getSession()->get('config')->senderEmail, $request->getSession()->get('config')->appName))
                        ->to($user->getEmail())
                        ->subject($request->getSession()->get('config')->appName . ' - ' . $this->translator->trans('emails.register.verify'))
                        ->htmlTemplate('emails/verifyAccount.html.twig')
                );
            } catch (\Exception $exception) {
                $this->addFlash('warning', $this->translator->trans('messages.errors.mails.send'));
            }

            $this->addFlash('success', $this->translator->trans('messages.oks.register.ok'));

            return $this->redirectToRoute('public_home');
        }

        return $this->render('public/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/verifyEmail", name="public_verifyEmail")
     */
    public function public_verifyEmail(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('danger', $exception->getReason());

            return $this->redirectToRoute('public_register');
        }

        $this->addFlash('success', $this->translator->trans('messages.oks.register.accountVerified'));

        return $this->redirectToRoute('public_home');
    }

    /**
     * @Route("/resetPassword", name="public_resetPassword")
     */
    public function public_resetPassword(Request $request, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ResetPasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processSendingPasswordResetEmail($form->get('email')->getData(), $request, $mailer);
        }

        return $this->render('public/resetPassword.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/resetPassword/checkEmail", name="public_resetPassword_checkEmail")
     */
    public function public_resetPassword_checkEmail(): Response
    {
        // We prevent users from directly accessing this page
        if (!$this->canCheckEmail()) {
            return $this->redirectToRoute('public_resetPassword');
        }

        return $this->render('public/resetPassword_checkEmail.html.twig', [
            'tokenLifetime' => $this->resetPasswordHelper->getTokenLifetime(),
        ]);
    }

    /**
     * @Route("/resetPassword/reset/{token}", name="public_resetPassword_reset")
     */
    public function public_resetPassword_reset(Request $request, UserPasswordEncoderInterface $passwordEncoder, string $token = null): Response
    {
        if ($token) {
            $this->storeTokenInSession($token);
            return $this->redirectToRoute('public_resetPassword_reset');
        }

        $token = $this->getTokenFromSession();
        if (null === $token) {
            $this->addFlash('danger', $this->translator->trans('messages.errors.resetPassword.noTokenURL'));
            return $this->redirectToRoute('public_resetPassword');
        }

        try {
            $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
        } catch (\Exception $exception) {
            $this->addFlash('danger', $this->translator->trans('messages.errors.resetPassword.noValidToken'));
            return $this->redirectToRoute('public_resetPassword');
        }

        // The token is valid; allow the user to change their password.
        $form = $this->createForm(ChangePasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // A password reset token should be used only once, remove it.
            $this->resetPasswordHelper->removeResetRequest($token);

            // Encode the plain password, and set it.
            $encodedPassword = $passwordEncoder->encodePassword($user, $form->get('plainPassword')->getData());
            $user->setPassword($encodedPassword);
            $this->getDoctrine()->getManager()->flush();

            // The session is cleaned up after the password has been changed.
            $this->cleanSessionAfterReset();

            $this->addFlash('success', $this->translator->trans('messages.oks.resetPassword.ok'));
            return $this->redirectToRoute('public_login');
        }

        return $this->render('public/resetPassword_reset.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function processSendingPasswordResetEmail(string $emailFormData, Request $request, MailerInterface $mailer): RedirectResponse
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $emailFormData,
        ]);

        // Marks that you are allowed to see the public_resetPassword_checkEmail page.
        $this->setCanCheckEmailInSession();

        // Do not reveal whether a user account was found or not.
        if (!$user) {
            return $this->redirectToRoute('public_resetPassword_checkEmail');
        }

        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (\Exception $exception) {
            $this->addFlash('danger', $this->translator->trans('messages.errors.resetPassword.generateToken'));
            return $this->redirectToRoute('public_resetPassword_checkEmail');
        }

        try {
            $email = (new TemplatedEmail())
                ->from(new Address($request->getSession()->get('config')->senderEmail, $request->getSession()->get('config')->appName))
                ->to($user->getEmail())
                ->subject($request->getSession()->get('config')->appName . ' - ' . $this->translator->trans('emails.resetPassword.reset'))
                ->htmlTemplate('emails/resetPassword.html.twig')
                ->context(['resetToken' => $resetToken, 'tokenLifetime' => $this->resetPasswordHelper->getTokenLifetime()]);
            $mailer->send($email);
        } catch (\Exception $exception) {
            $this->addFlash('warning', $this->translator->trans('messages.errors.mails.send'));
        }

        return $this->redirectToRoute('public_resetPassword_checkEmail');
    }
}
