<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

use Doctrine\ORM\EntityManagerInterface;

abstract class CustomAbstractCrudController extends AbstractCrudController
{

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $className = str_replace("App\\Entity\\", "", get_class($entityInstance));
        $entityNameTrans = $this->translator->trans('entities.' . strtolower($className) . '.singular');
        $message = $this->translator->trans('messages.oks.crud.create', array('%entity%' => $entityNameTrans));

        $this->addFlash('success', $message);
        parent::persistEntity($entityManager, $entityInstance);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $className = str_replace("App\\Entity\\", "", get_class($entityInstance));
        $entityNameTrans = $this->translator->trans('entities.' . strtolower($className) . '.singular');
        $message = $this->translator->trans('messages.oks.crud.update', array('%entity%' => $entityNameTrans));

        $this->addFlash('success', $message);
        parent::updateEntity($entityManager, $entityInstance);
    }

    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $className = str_replace("App\\Entity\\", "", get_class($entityInstance));
        $entityNameTrans = $this->translator->trans('entities.' . strtolower($className) . '.singular');
        $message = $this->translator->trans('messages.oks.crud.delete', array('%entity%' => $entityNameTrans));

        $this->addFlash('success', $message);
        parent::deleteEntity($entityManager, $entityInstance);
    }

}