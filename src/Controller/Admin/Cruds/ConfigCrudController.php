<?php

namespace App\Controller\Admin\Cruds;

use App\Controller\Admin\CustomAbstractCrudController;

use Symfony\Contracts\Translation\TranslatorInterface;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use App\Helper\FieldGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;

use App\Entity\Config;

class ConfigCrudController extends CustomAbstractCrudController
{
    public $translator;
    public $adminUrlGenerator;

    public function __construct(TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator)
    {
        $this->translator = $translator;
        $this->adminUrlGenerator = $adminUrlGenerator;
    }

    public static function getEntityFqcn(): string
    {
        return Config::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.config.singular'));
        $crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('entities.config.singular'));
        $crud->setEntityLabelInSingular($this->translator->trans('entities.config.singular'));

        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        $appName = FieldGenerator::text('appName', [
            'label' => $this->translator->trans('entities.config.fields.appName'),
            'data' => $this->container->get('session')->get('config')->appName,
            'attr' => [
                'placeholder' => $this->translator->trans('entities.config.fields.appName')
            ]
        ]);
        $appLogo = FieldGenerator::media('appLogo', [
            'label' => $this->translator->trans('entities.config.fields.appLogo'),
            'translator' => $this->translator,
            'acceptedFiles' => ".png, .jpeg, .jpg"
        ]);
        $appHeaderLogo = FieldGenerator::media('appHeaderLogo', [
            'label' => $this->translator->trans('entities.config.fields.appHeaderLogo'),
            'translator' => $this->translator,
            'acceptedFiles' => ".png, .jpeg, .jpg"
        ])->setHelp($this->translator->trans('entities.config.fields.appHeaderLogo_help'));
        $appFavicon = FieldGenerator::media('appFavicon', [
            'label' => $this->translator->trans('entities.config.fields.appFavicon'),
            'translator' => $this->translator,
            'acceptedFiles' => ".ico, .png"
        ]);
        $floatingAlerts = FieldGenerator::checkbox('floatingAlerts', [
            'label' => $this->translator->trans('entities.config.fields.floatingAlerts'),
            'data' => $this->container->get('session')->get('config')->floatingAlerts
        ]);
        $enableUsername = FieldGenerator::checkbox('enableUsername', [
            'label' => $this->translator->trans('entities.config.fields.enableUsername'),
            'data' => $this->container->get('session')->get('config')->enableUsername
        ]);
        $enablePublicLogin = FieldGenerator::checkbox('enablePublicLogin', [
            'label' => $this->translator->trans('entities.config.fields.enablePublicLogin'),
            'data' => $this->container->get('session')->get('config')->enablePublicLogin
        ]);
        $enableRegister = FieldGenerator::checkbox('enableRegister', [
            'label' => $this->translator->trans('entities.config.fields.enableRegister'),
            'data' => $this->container->get('session')->get('config')->enableRegister
        ]);
        $enablePublicHeader = FieldGenerator::checkbox('enablePublicHeader', [
            'label' => $this->translator->trans('entities.config.fields.enablePublicHeader'),
            'data' => $this->container->get('session')->get('config')->enablePublicHeader
        ]);
        $enablePublicFooter = FieldGenerator::checkbox('enablePublicFooter', [
            'label' => $this->translator->trans('entities.config.fields.enablePublicFooter'),
            'data' => $this->container->get('session')->get('config')->enablePublicFooter
        ]);
        $senderEmail = FieldGenerator::email('senderEmail', [
            'label' => $this->translator->trans('entities.config.fields.senderEmail'),
            'data' => $this->container->get('session')->get('config')->senderEmail,
            'attr' => [
                'placeholder' => $this->translator->trans('entities.config.fields.senderEmail')
            ]
        ]);
        $locale = FieldGenerator::select('locale', [
            'label' => $this->translator->trans('entities.config.fields.locale'),
            'data' => $this->container->get('session')->get('config')->locale,
            'choices' => [
                '<i class="flag-icon flag-icon-us"></i> ' . "English" => "en",
                '<i class="flag-icon flag-icon-es"></i> ' . "Spanish" => "es",
            ],
            'attr' => [
                'placeholder' => $this->translator->trans('entities.config.fields.locale')
            ],
            'required' => false
        ]);
        $timezones = array();
        foreach (timezone_identifiers_list() as $value) {
            $timezones[$value] = $value;
        }
        $timezone = FieldGenerator::select('timezone', [
            'label' => $this->translator->trans('entities.config.fields.timezone'),
            'data' => $this->container->get('session')->get('config')->timezone,
            'choices' => $timezones,
            'attr' => [
                'placeholder' => $this->translator->trans('entities.config.fields.timezone')
            ],
            'required' => false
        ]);
        $entityActionsAsDropdown = FieldGenerator::checkbox('entityActionsAsDropdown', [
            'label' => $this->translator->trans('entities.config.fields.entityActionsAsDropdown'),
            'data' => $this->container->get('session')->get('config')->entityActionsAsDropdown
        ]);
        $paginatorPageSize = FieldGenerator::number('paginatorPageSize', [
            'label' => $this->translator->trans('entities.config.fields.paginatorPageSize'),
            'data' => $this->container->get('session')->get('config')->paginatorPageSize,
            'attr' => [
                'min' => 0,
                'max' => 250
            ]
        ]);
        $panelGeneral = FormField::addPanel($this->translator->trans('entities.config.sections.general'))->setIcon('fas fa-fw fa-cogs');
        $panelDesign = FormField::addPanel($this->translator->trans('entities.config.sections.design'))->setIcon('fas fa-fw fa-cogs');

        //Check panels and fields visibility
        $show_enablePublicLoginAndRegister = $this->getParameter('enabled_public') && $this->getUser()->hasPermission('EnableLoginAndRegister');

        $fields = array();
        if ($pageName == Crud::PAGE_INDEX) {
            array_push($fields, $appLogo);
            array_push($fields, $appHeaderLogo);
            array_push($fields, $appFavicon);
            array_push($fields, $appName);
            array_push($fields, $floatingAlerts);
            array_push($fields, $enableUsername);
            if ($show_enablePublicLoginAndRegister) {
                array_push($fields, $enablePublicLogin);
                array_push($fields, $enableRegister);
            }
            if ($this->getParameter('enabled_public')) {
                array_push($fields, $enablePublicHeader);
                array_push($fields, $enablePublicFooter);
            }
            array_push($fields, $senderEmail);
            array_push($fields, $locale);
            array_push($fields, $timezone);
            array_push($fields, $entityActionsAsDropdown);
            array_push($fields, $paginatorPageSize);
        } else if ($pageName == Crud::PAGE_DETAIL) {
            array_push($fields, $panelGeneral);
            array_push($fields, $appName);
            array_push($fields, $enableUsername);
            if ($show_enablePublicLoginAndRegister) {
                array_push($fields, $enablePublicLogin);
                array_push($fields, $enableRegister);
            }
            if ($this->getParameter('enabled_public')) {
                array_push($fields, $enablePublicHeader);
                array_push($fields, $enablePublicFooter);
            }
            array_push($fields, $senderEmail);
            array_push($fields, $locale);
            array_push($fields, $timezone);
            array_push($fields, $panelDesign);
            array_push($fields, $entityActionsAsDropdown);
            array_push($fields, $paginatorPageSize);
            array_push($fields, $floatingAlerts);
            array_push($fields, $appLogo);
            array_push($fields, $appHeaderLogo);
            array_push($fields, $appFavicon);
        } else if ($pageName == Crud::PAGE_NEW) {
            array_push($fields, $panelGeneral);
            array_push($fields, $appName);
            array_push($fields, $enableUsername);
            if ($show_enablePublicLoginAndRegister) {
                array_push($fields, $enablePublicLogin);
                array_push($fields, $enableRegister);
            }
            if ($this->getParameter('enabled_public')) {
                array_push($fields, $enablePublicHeader);
                array_push($fields, $enablePublicFooter);
            }
            array_push($fields, $senderEmail);
            array_push($fields, $locale);
            array_push($fields, $timezone);
            array_push($fields, $panelDesign);
            array_push($fields, $entityActionsAsDropdown);
            array_push($fields, $paginatorPageSize);
            array_push($fields, $floatingAlerts);
            array_push($fields, $appLogo);
            array_push($fields, $appHeaderLogo);
            array_push($fields, $appFavicon);
        } else if ($pageName == Crud::PAGE_EDIT) {
            array_push($fields, $panelGeneral);
            array_push($fields, $appName);
            array_push($fields, $enableUsername);
            if ($show_enablePublicLoginAndRegister) {
                array_push($fields, $enablePublicLogin);
                array_push($fields, $enableRegister);
            }
            if ($this->getParameter('enabled_public')) {
                array_push($fields, $enablePublicHeader);
                array_push($fields, $enablePublicFooter);
            }
            array_push($fields, $senderEmail);
            array_push($fields, $locale);
            array_push($fields, $timezone);
            array_push($fields, $panelDesign);
            array_push($fields, $entityActionsAsDropdown);
            array_push($fields, $paginatorPageSize);
            array_push($fields, $floatingAlerts);
            array_push($fields, $appLogo);
            array_push($fields, $appHeaderLogo);
            array_push($fields, $appFavicon);
        }

        return $fields;
    }

    public function configureActions(Actions $actions): Actions
    {
        $user = $this->getUser();

        $actions->remove(Crud::PAGE_NEW, Action::INDEX);
        $actions->remove(Crud::PAGE_NEW, Action::SAVE_AND_RETURN);
        $actions->add(Crud::PAGE_NEW, Action::SAVE_AND_CONTINUE);
        $actions->update(Crud::PAGE_NEW, Action::SAVE_AND_CONTINUE, function (Action $action) {
            return $action->setIcon('fas fa-fw fa-save')->addCssClass('btn-success btn-sm btn-loader')->setLabel($this->translator->trans('__ea__action.save'));
        });

        $actions->remove(Crud::PAGE_DETAIL, Action::INDEX);
        $actions->remove(Crud::PAGE_DETAIL, Action::DELETE);

        $actions->remove(Crud::PAGE_EDIT, Action::INDEX);
        $actions->remove(Crud::PAGE_EDIT, Action::DELETE);
        
        $actions->reorder(Crud::PAGE_NEW, [Action::SAVE_AND_CONTINUE]);
        $actions->reorder(Crud::PAGE_DETAIL, [Action::EDIT]);
        $actions->reorder(Crud::PAGE_EDIT, [Action::SAVE_AND_RETURN]);
        
        if (!$user->hasPermission('EditConfig')) {
            $actions->remove(Crud::PAGE_DETAIL, Action::EDIT);
        }

        return $actions;
    }
    
    public function new(\EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext $context) {
        $redirect = parent::new($context);
        if ($redirect instanceof \Symfony\Component\HttpFoundation\RedirectResponse) {
            $em = $this->getDoctrine()->getManager();
            $config = $em->getRepository(Config::class)->findOneBy(array(), array('id' => 'DESC'));

            $url = $this->adminUrlGenerator
            ->setAction(Crud::PAGE_DETAIL)->setEntityId($config->getId())
            ->generateUrl();

            return $this->redirect($url);
        }
        return $redirect;
    }
}
