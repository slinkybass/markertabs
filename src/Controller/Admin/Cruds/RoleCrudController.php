<?php

namespace App\Controller\Admin\Cruds;

use App\Controller\Admin\CustomAbstractCrudController;

use Symfony\Contracts\Translation\TranslatorInterface;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;

use App\Helper\FieldGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;

use App\Entity\Role;

class RoleCrudController extends CustomAbstractCrudController
{
    public $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getEntityFqcn(): string
    {
        return Role::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setEntityLabelInPlural($this->translator->trans('entities.role.plural'));
        $crud->setEntityLabelInSingular($this->translator->trans('entities.role.singular'));
        $crud->setDefaultSort(['name' => 'ASC']);

        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        $id = FieldGenerator::text('id');
        $name = FieldGenerator::text('name', [
            'label' => $this->translator->trans('entities.role.fields.name'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.role.fields.name')
            ]
        ]);
        $permission_showMedia = FieldGenerator::checkbox('permission_showMedia', [
            'label' => $this->translator->trans('entities.role.fields.permission_showMedia'),
            'attr' => [
                'data-checkbox-parent' => "showMedia"
            ]
        ]);
        $permission_editMedia = FieldGenerator::checkbox('permission_editMedia', [
            'label' => $this->translator->trans('entities.role.fields.permission_editMedia'),
            'attr' => [
                'data-checkbox-parent' => "editMedia",
                'data-checkbox-child' => "showMedia"
            ]
        ]);
        $permission_deleteMedia = FieldGenerator::checkbox('permission_deleteMedia', [
            'label' => $this->translator->trans('entities.role.fields.permission_deleteMedia'),
            'attr' => [
                'data-checkbox-child' => "editMedia"
            ]
        ]);
        $permission_showUser = FieldGenerator::checkbox('permission_showUser', [
            'label' => $this->translator->trans('entities.role.fields.permission_showUser'),
            'attr' => [
                'data-checkbox-parent' => "showUser"
            ]
        ]);
        $permission_createUser = FieldGenerator::checkbox('permission_createUser', [
            'label' => $this->translator->trans('entities.role.fields.permission_createUser'),
            'attr' => [
                'data-checkbox-child' => "showUser"
            ]
        ]);
        $permission_editUser = FieldGenerator::checkbox('permission_editUser', [
            'label' => $this->translator->trans('entities.role.fields.permission_editUser'),
            'attr' => [
                'data-checkbox-parent' => "editUser",
                'data-checkbox-child' => "showUser"
            ]
        ]);
        $permission_deleteUser = FieldGenerator::checkbox('permission_deleteUser', [
            'label' => $this->translator->trans('entities.role.fields.permission_deleteUser'),
            'attr' => [
                'data-checkbox-child' => "editUser"
            ]
        ]);
        $permission_showUserAdmin = FieldGenerator::checkbox('permission_showUserAdmin', [
            'label' => $this->translator->trans('entities.role.fields.permission_showUserAdmin'),
            'attr' => [
                'data-checkbox-parent' => "showUserAdmin"
            ]
        ]);
        $permission_createUserAdmin = FieldGenerator::checkbox('permission_createUserAdmin', [
            'label' => $this->translator->trans('entities.role.fields.permission_createUserAdmin'),
            'attr' => [
                'data-checkbox-parent' => "createUserAdmin",
                'data-checkbox-child' => "showUserAdmin"
            ]
        ]);
        $permission_editUserAdmin = FieldGenerator::checkbox('permission_editUserAdmin', [
            'label' => $this->translator->trans('entities.role.fields.permission_editUserAdmin'),
            'attr' => [
                'data-checkbox-parent' => "editUserAdmin",
                'data-checkbox-child' => "showUserAdmin"
            ]
        ]);
        $permission_deleteUserAdmin = FieldGenerator::checkbox('permission_deleteUserAdmin', [
            'label' => $this->translator->trans('entities.role.fields.permission_deleteUserAdmin'),
            'attr' => [
                'data-checkbox-child' => "editUserAdmin"
            ]
        ]);
        $permission_createUserSuperadmin = FieldGenerator::checkbox('permission_createUserSuperadmin', [
            'label' => $this->translator->trans('entities.role.fields.permission_createUserSuperadmin'),
            'attr' => [
                'data-checkbox-child' => "createUserAdmin"
            ]
        ]);
        $permission_editUserSuperadmin = FieldGenerator::checkbox('permission_editUserSuperadmin', [
            'label' => $this->translator->trans('entities.role.fields.permission_editUserSuperadmin'),
            'attr' => [
                'data-checkbox-parent' => "editUserSuperadmin",
                'data-checkbox-child' => "editUserAdmin"
            ]
        ]);
        $permission_deleteUserSuperadmin = FieldGenerator::checkbox('permission_deleteUserSuperadmin', [
            'label' => $this->translator->trans('entities.role.fields.permission_deleteUserSuperadmin'),
            'attr' => [
                'data-checkbox-child' => "editUserSuperadmin"
            ]
        ]);
        $permission_showRole = FieldGenerator::checkbox('permission_showRole', [
            'label' => $this->translator->trans('entities.role.fields.permission_showRole'),
            'attr' => [
                'data-checkbox-parent' => "showRole"
            ]
        ]);
        $permission_createRole = FieldGenerator::checkbox('permission_createRole', [
            'label' => $this->translator->trans('entities.role.fields.permission_createRole'),
            'attr' => [
                'data-checkbox-child' => "showRole"
            ]
        ]);
        $permission_editRole = FieldGenerator::checkbox('permission_editRole', [
            'label' => $this->translator->trans('entities.role.fields.permission_editRole'),
            'attr' => [
                'data-checkbox-parent' => "editRole",
                'data-checkbox-child' => "showRole"
            ]
        ]);
        $permission_deleteRole = FieldGenerator::checkbox('permission_deleteRole', [
            'label' => $this->translator->trans('entities.role.fields.permission_deleteRole'),
            'attr' => [
                'data-checkbox-child' => "editRole"
            ]
        ]);
        $permission_showRoleSuperadmin = FieldGenerator::checkbox('permission_showRoleSuperadmin', [
            'label' => $this->translator->trans('entities.role.fields.permission_showRoleSuperadmin'),
            'attr' => [
                'data-checkbox-parent' => "showRoleSuperadmin",
                'data-checkbox-child' => "showRole"
            ]
        ]);
        $permission_editRoleSuperadmin = FieldGenerator::checkbox('permission_editRoleSuperadmin', [
            'label' => $this->translator->trans('entities.role.fields.permission_editRoleSuperadmin'),
            'attr' => [
                'data-checkbox-child' => "showRoleSuperadmin"
            ]
        ]);
        $permission_showRoleUser = FieldGenerator::checkbox('permission_showRoleUser', [
            'label' => $this->translator->trans('entities.role.fields.permission_showRoleUser'),
            'attr' => [
                'data-checkbox-parent' => "showRoleUser",
                'data-checkbox-child' => "showRole"
            ]
        ]);
        $permission_editRoleUser = FieldGenerator::checkbox('permission_editRoleUser', [
            'label' => $this->translator->trans('entities.role.fields.permission_editRoleUser'),
            'attr' => [
                'data-checkbox-child' => "showRoleUser"
            ]
        ]);
        $permission_editRoleSelf = FieldGenerator::checkbox('permission_editRoleSelf', [
            'label' => $this->translator->trans('entities.role.fields.permission_editRoleSelf'),
            'attr' => [
                'data-checkbox-parent' => "editRoleSelf",
                'data-checkbox-child' => "showRole"
            ]
        ]);
        $permission_deleteRoleSelf = FieldGenerator::checkbox('permission_deleteRoleSelf', [
            'label' => $this->translator->trans('entities.role.fields.permission_deleteRoleSelf'),
            'attr' => [
                'data-checkbox-child' => "editRoleSelf"
            ]
        ]);
        $permission_showConfig = FieldGenerator::checkbox('permission_showConfig', [
            'label' => $this->translator->trans('entities.role.fields.permission_showConfig'),
            'attr' => [
                'data-checkbox-parent' => "showConfig",
            ]
        ]);
        $permission_editConfig = FieldGenerator::checkbox('permission_editConfig', [
            'label' => $this->translator->trans('entities.role.fields.permission_editConfig'),
            'attr' => [
                'data-checkbox-parent' => "editConfig",
                'data-checkbox-child' => "showConfig"
            ]
        ]);
        $permission_enableLoginAndRegister = FieldGenerator::checkbox('permission_enableLoginAndRegister', [
            'label' => $this->translator->trans('entities.role.fields.permission_enableLoginAndRegister'),
            'attr' => [
                'data-checkbox-child' => "editConfig",
            ]
        ]);
        $permission_showTab = FieldGenerator::checkbox('permission_showTab', [
            'label' => $this->translator->trans('entities.tab.fields.permission_showTab'),
            'attr' => [
                'data-checkbox-parent' => "showTab"
            ]
        ]);
        $permission_createTab = FieldGenerator::checkbox('permission_createTab', [
            'label' => $this->translator->trans('entities.tab.fields.permission_createTab'),
            'attr' => [
                'data-checkbox-child' => "showTab"
            ]
        ]);
        $permission_editTab = FieldGenerator::checkbox('permission_editTab', [
            'label' => $this->translator->trans('entities.tab.fields.permission_editTab'),
            'attr' => [
                'data-checkbox-parent' => "editTab",
                'data-checkbox-child' => "showTab"
            ]
        ]);
        $permission_deleteTab = FieldGenerator::checkbox('permission_deleteTab', [
            'label' => $this->translator->trans('entities.tab.fields.permission_deleteTab'),
            'attr' => [
                'data-checkbox-child' => "editTab"
            ]
        ]);
        $permission_showLink = FieldGenerator::checkbox('permission_showLink', [
            'label' => $this->translator->trans('entities.link.fields.permission_showLink'),
            'attr' => [
                'data-checkbox-parent' => "showLink"
            ]
        ]);
        $permission_createLink = FieldGenerator::checkbox('permission_createLink', [
            'label' => $this->translator->trans('entities.link.fields.permission_createLink'),
            'attr' => [
                'data-checkbox-child' => "showLink"
            ]
        ]);
        $permission_editLink = FieldGenerator::checkbox('permission_editLink', [
            'label' => $this->translator->trans('entities.link.fields.permission_editLink'),
            'attr' => [
                'data-checkbox-parent' => "editLink",
                'data-checkbox-child' => "showLink"
            ]
        ]);
        $permission_deleteLink = FieldGenerator::checkbox('permission_deleteLink', [
            'label' => $this->translator->trans('entities.link.fields.permission_deleteLink'),
            'attr' => [
                'data-checkbox-child' => "editLink"
            ]
        ]);
        $panelData = FormField::addPanel($this->translator->trans('entities.role.sections.data'))->setIcon('fas fa-fw fa-cogs');
        $panelPermissionsMedia = FormField::addPanel($this->translator->trans('entities.role.sections.permissions_media'))->setIcon('fas fa-fw fa-lock');
        $panelPermissionsUser = FormField::addPanel($this->translator->trans('entities.role.sections.permissions_user'))->setIcon('fas fa-fw fa-lock');
        $panelPermissionsRole = FormField::addPanel($this->translator->trans('entities.role.sections.permissions_role'))->setIcon('fas fa-fw fa-lock');
        $panelPermissionsConfig = FormField::addPanel($this->translator->trans('entities.role.sections.permissions_config'))->setIcon('fas fa-fw fa-lock');
        $panelPermissionsTab = FormField::addPanel($this->translator->trans('entities.role.sections.permissions_tab'))->setIcon('fas fa-fw fa-lock');
        $panelPermissionsLink = FormField::addPanel($this->translator->trans('entities.role.sections.permissions_link'))->setIcon('fas fa-fw fa-lock');

        //Check panels and fields visibility
        $show_permission_showMedia = $this->getUser()->hasPermission('ShowMedia');
        $show_permission_editMedia = $this->getUser()->hasPermission('EditMedia');
        $show_permission_deleteMedia = $this->getUser()->hasPermission('DeleteMedia');
        $show_permission_showUser = $this->getParameter('enabled_public') && $this->getUser()->hasPermission('ShowUser');
        $show_permission_createUser = $this->getParameter('enabled_public') && $this->getUser()->hasPermission('CreateUser');
        $show_permission_editUser = $this->getParameter('enabled_public') && $this->getUser()->hasPermission('EditUser');
        $show_permission_deleteUser = $this->getParameter('enabled_public') && $this->getUser()->hasPermission('DeleteUser');
        $show_permission_showUserAdmin = $this->getUser()->hasPermission('ShowUserAdmin');
        $show_permission_createUserAdmin = $this->getUser()->hasPermission('CreateUserAdmin');
        $show_permission_editUserAdmin = $this->getUser()->hasPermission('EditUserAdmin');
        $show_permission_deleteUserAdmin = $this->getUser()->hasPermission('DeleteUserAdmin');
        $show_permission_createUserSuperadmin = $this->getUser()->hasPermission('CreateUserSuperadmin');
        $show_permission_editUserSuperadmin = $this->getUser()->hasPermission('EditUserSuperadmin');
        $show_permission_deleteUserSuperadmin = $this->getUser()->hasPermission('DeleteUserSuperadmin');
        $show_permission_showRole = $this->getUser()->hasPermission('ShowRole');
        $show_permission_createRole = $this->getUser()->hasPermission('CreateRole');
        $show_permission_editRole = $this->getUser()->hasPermission('EditRole');
        $show_permission_deleteRole = $this->getUser()->hasPermission('DeleteRole');
        $show_permission_showRoleSuperadmin = $this->getUser()->hasPermission('ShowRoleSuperadmin');
        $show_permission_editRoleSuperadmin = $this->getUser()->hasPermission('EditRoleSuperadmin');
        $show_permission_showRoleUser = $this->getUser()->hasPermission('ShowRoleUser');
        $show_permission_editRoleUser = $this->getUser()->hasPermission('EditRoleUser');
        $show_permission_editRoleSelf = $this->getUser()->hasPermission('EditRoleSelf');
        $show_permission_deleteRoleSelf = $this->getUser()->hasPermission('DeleteRoleSelf');
        $show_permission_showConfig = $this->getUser()->hasPermission('ShowConfig');
        $show_permission_editConfig = $this->getUser()->hasPermission('EditConfig');
        $show_permission_enableLoginAndRegister = $this->getParameter('enabled_public') && $this->getUser()->hasPermission('EnableLoginAndRegister');
        $show_permission_showTab = $this->getUser()->hasPermission('ShowTab');
        $show_permission_createTab = $this->getUser()->hasPermission('CreateTab');
        $show_permission_editTab = $this->getUser()->hasPermission('EditTab');
        $show_permission_deleteTab = $this->getUser()->hasPermission('DeleteTab');
        $show_permission_showLink = $this->getUser()->hasPermission('ShowLink');
        $show_permission_createLink = $this->getUser()->hasPermission('CreateLink');
        $show_permission_editLink = $this->getUser()->hasPermission('EditLink');
        $show_permission_deleteLink = $this->getUser()->hasPermission('DeleteLink');
        
        $fields = array();
        if ($pageName == Crud::PAGE_INDEX) {
            array_push($fields, $name);
            array_push($fields, $id);
        } else if ($pageName == Crud::PAGE_DETAIL) {
            array_push($fields, $panelData);
            array_push($fields, $id);
            array_push($fields, $name);
            if ($show_permission_showMedia || $show_permission_editMedia || $show_permission_deleteMedia)  {
                array_push($fields, $panelPermissionsMedia);
                if ($show_permission_showMedia)  {
                    array_push($fields, $permission_showMedia);
                }
                if ($show_permission_editMedia)  {
                    array_push($fields, $permission_editMedia);
                }
                if ($show_permission_deleteMedia)  {
                    array_push($fields, $permission_deleteMedia);
                }
            }
            if ($show_permission_showUser || $show_permission_createUser || $show_permission_editUser || $show_permission_deleteUser || 
                $show_permission_showUserAdmin || $show_permission_createUserAdmin || $show_permission_editUserAdmin || $show_permission_deleteUserAdmin || 
                $show_permission_createUserSuperadmin || $show_permission_editUserSuperadmin || $show_permission_deleteUserSuperadmin)  {
                array_push($fields, $panelPermissionsUser);
                if ($show_permission_showUser)  {
                    array_push($fields, $permission_showUser);
                }
                if ($show_permission_createUser)  {
                    array_push($fields, $permission_createUser);
                }
                if ($show_permission_editUser)  {
                    array_push($fields, $permission_editUser);
                }
                if ($show_permission_deleteUser)  {
                    array_push($fields, $permission_deleteUser);
                }
                if ($show_permission_showUserAdmin)  {
                    array_push($fields, $permission_showUserAdmin);
                }
                if ($show_permission_createUserAdmin)  {
                    array_push($fields, $permission_createUserAdmin);
                }
                if ($show_permission_editUserAdmin)  {
                    array_push($fields, $permission_editUserAdmin);
                }
                if ($show_permission_deleteUserAdmin)  {
                    array_push($fields, $permission_deleteUserAdmin);
                }
                if ($show_permission_createUserSuperadmin)  {
                    array_push($fields, $permission_createUserSuperadmin);
                }
                if ($show_permission_editUserSuperadmin)  {
                    array_push($fields, $permission_editUserSuperadmin);
                }
                if ($show_permission_deleteUserSuperadmin)  {
                    array_push($fields, $permission_deleteUserSuperadmin);
                }
            }
            if ($show_permission_showRole || $show_permission_createRole || $show_permission_editRole || $show_permission_deleteRole ||
                $show_permission_showRoleSuperadmin || $show_permission_showRoleUser || $show_permission_editRoleSelf || $show_permission_deleteRoleSelf)  {
                array_push($fields, $panelPermissionsRole);
                if ($show_permission_showRole)  {
                    array_push($fields, $permission_showRole);
                }
                if ($show_permission_createRole)  {
                    array_push($fields, $permission_createRole);
                }
                if ($show_permission_editRole)  {
                    array_push($fields, $permission_editRole);
                }
                if ($show_permission_deleteRole)  {
                    array_push($fields, $permission_deleteRole);
                }
                if ($show_permission_showRoleSuperadmin)  {
                    array_push($fields, $permission_showRoleSuperadmin);
                }
                if ($show_permission_editRoleSuperadmin)  {
                    array_push($fields, $permission_editRoleSuperadmin);
                }
                if ($show_permission_showRoleUser)  {
                    array_push($fields, $permission_showRoleUser);
                }
                if ($show_permission_editRoleUser)  {
                    array_push($fields, $permission_editRoleUser);
                }
                if ($show_permission_editRoleSelf)  {
                    array_push($fields, $permission_editRoleSelf);
                }
                if ($show_permission_deleteRoleSelf)  {
                    array_push($fields, $permission_deleteRoleSelf);
                }
            }
            if ($show_permission_showConfig || $show_permission_editConfig)  {
                array_push($fields, $panelPermissionsConfig);
                if ($show_permission_showConfig)  {
                    array_push($fields, $permission_showConfig);
                }
                if ($show_permission_editConfig)  {
                    array_push($fields, $permission_editConfig);
                }
				if ($show_permission_enableLoginAndRegister)  {
					array_push($fields, $permission_enableLoginAndRegister);
				}
            }
            if ($show_permission_showTab || $show_permission_createTab || $show_permission_editTab || $show_permission_deleteTab)  {
                array_push($fields, $panelPermissionsTab);
                if ($show_permission_showTab)  {
                    array_push($fields, $permission_showTab);
                }
                if ($show_permission_createTab)  {
                    array_push($fields, $permission_createTab);
                }
                if ($show_permission_editTab)  {
                    array_push($fields, $permission_editTab);
                }
                if ($show_permission_deleteTab)  {
                    array_push($fields, $permission_deleteTab);
                }
            }
            if ($show_permission_showLink || $show_permission_createLink || $show_permission_editLink || $show_permission_deleteLink)  {
                array_push($fields, $panelPermissionsLink);
                if ($show_permission_showLink)  {
                    array_push($fields, $permission_showLink);
                }
                if ($show_permission_createLink)  {
                    array_push($fields, $permission_createLink);
                }
                if ($show_permission_editLink)  {
                    array_push($fields, $permission_editLink);
                }
                if ($show_permission_deleteLink)  {
                    array_push($fields, $permission_deleteLink);
                }
            }
        } else if ($pageName == Crud::PAGE_NEW) {
            array_push($fields, $panelData);
            array_push($fields, $name);
            if ($show_permission_showMedia || $show_permission_editMedia || $show_permission_deleteMedia)  {
                array_push($fields, $panelPermissionsMedia);
                if ($show_permission_showMedia)  {
                    array_push($fields, $permission_showMedia);
                }
                if ($show_permission_editMedia)  {
                    array_push($fields, $permission_editMedia);
                }
                if ($show_permission_deleteMedia)  {
                    array_push($fields, $permission_deleteMedia);
                }
            }
            if ($show_permission_showUser || $show_permission_createUser || $show_permission_editUser || $show_permission_deleteUser || 
                $show_permission_showUserAdmin || $show_permission_createUserAdmin || $show_permission_editUserAdmin || $show_permission_deleteUserAdmin || 
                $show_permission_createUserSuperadmin || $show_permission_editUserSuperadmin || $show_permission_deleteUserSuperadmin)  {
                array_push($fields, $panelPermissionsUser);
                if ($show_permission_showUser)  {
                    array_push($fields, $permission_showUser);
                }
                if ($show_permission_createUser)  {
                    array_push($fields, $permission_createUser);
                }
                if ($show_permission_editUser)  {
                    array_push($fields, $permission_editUser);
                }
                if ($show_permission_deleteUser)  {
                    array_push($fields, $permission_deleteUser);
                }
                if ($show_permission_showUserAdmin)  {
                    array_push($fields, $permission_showUserAdmin);
                }
                if ($show_permission_createUserAdmin)  {
                    array_push($fields, $permission_createUserAdmin);
                }
                if ($show_permission_editUserAdmin)  {
                    array_push($fields, $permission_editUserAdmin);
                }
                if ($show_permission_deleteUserAdmin)  {
                    array_push($fields, $permission_deleteUserAdmin);
                }
                if ($show_permission_createUserSuperadmin)  {
                    array_push($fields, $permission_createUserSuperadmin);
                }
                if ($show_permission_editUserSuperadmin)  {
                    array_push($fields, $permission_editUserSuperadmin);
                }
                if ($show_permission_deleteUserSuperadmin)  {
                    array_push($fields, $permission_deleteUserSuperadmin);
                }
            }
            if ($show_permission_showRole || $show_permission_createRole || $show_permission_editRole || $show_permission_deleteRole ||
                $show_permission_showRoleSuperadmin || $show_permission_showRoleUser || $show_permission_editRoleSelf || $show_permission_deleteRoleSelf)  {
                array_push($fields, $panelPermissionsRole);
                if ($show_permission_showRole)  {
                    array_push($fields, $permission_showRole);
                }
                if ($show_permission_createRole)  {
                    array_push($fields, $permission_createRole);
                }
                if ($show_permission_editRole)  {
                    array_push($fields, $permission_editRole);
                }
                if ($show_permission_deleteRole)  {
                    array_push($fields, $permission_deleteRole);
                }
                if ($show_permission_showRoleSuperadmin)  {
                    array_push($fields, $permission_showRoleSuperadmin);
                }
                if ($show_permission_editRoleSuperadmin)  {
                    array_push($fields, $permission_editRoleSuperadmin);
                }
                if ($show_permission_showRoleUser)  {
                    array_push($fields, $permission_showRoleUser);
                }
                if ($show_permission_editRoleUser)  {
                    array_push($fields, $permission_editRoleUser);
                }
                if ($show_permission_editRoleSelf)  {
                    array_push($fields, $permission_editRoleSelf);
                }
                if ($show_permission_deleteRoleSelf)  {
                    array_push($fields, $permission_deleteRoleSelf);
                }
            }
            if ($show_permission_showConfig || $show_permission_editConfig)  {
                array_push($fields, $panelPermissionsConfig);
                if ($show_permission_showConfig)  {
                    array_push($fields, $permission_showConfig);
                }
                if ($show_permission_editConfig)  {
                    array_push($fields, $permission_editConfig);
                }
				if ($show_permission_enableLoginAndRegister)  {
					array_push($fields, $permission_enableLoginAndRegister);
				}
            }
            if ($show_permission_showTab || $show_permission_createTab || $show_permission_editTab || $show_permission_deleteTab)  {
                array_push($fields, $panelPermissionsTab);
                if ($show_permission_showTab)  {
                    array_push($fields, $permission_showTab);
                }
                if ($show_permission_createTab)  {
                    array_push($fields, $permission_createTab);
                }
                if ($show_permission_editTab)  {
                    array_push($fields, $permission_editTab);
                }
                if ($show_permission_deleteTab)  {
                    array_push($fields, $permission_deleteTab);
                }
            }
            if ($show_permission_showLink || $show_permission_createLink || $show_permission_editLink || $show_permission_deleteLink)  {
                array_push($fields, $panelPermissionsLink);
                if ($show_permission_showLink)  {
                    array_push($fields, $permission_showLink);
                }
                if ($show_permission_createLink)  {
                    array_push($fields, $permission_createLink);
                }
                if ($show_permission_editLink)  {
                    array_push($fields, $permission_editLink);
                }
                if ($show_permission_deleteLink)  {
                    array_push($fields, $permission_deleteLink);
                }
            }
        } else if ($pageName == Crud::PAGE_EDIT) {
            array_push($fields, $panelData);
            array_push($fields, $name);
            if ($show_permission_showMedia || $show_permission_editMedia || $show_permission_deleteMedia)  {
                array_push($fields, $panelPermissionsMedia);
                if ($show_permission_showMedia)  {
                    array_push($fields, $permission_showMedia);
                }
                if ($show_permission_editMedia)  {
                    array_push($fields, $permission_editMedia);
                }
                if ($show_permission_deleteMedia)  {
                    array_push($fields, $permission_deleteMedia);
                }
            }
            if ($show_permission_showUser || $show_permission_createUser || $show_permission_editUser || $show_permission_deleteUser || 
                $show_permission_showUserAdmin || $show_permission_createUserAdmin || $show_permission_editUserAdmin || $show_permission_deleteUserAdmin || 
                $show_permission_createUserSuperadmin || $show_permission_editUserSuperadmin || $show_permission_deleteUserSuperadmin)  {
                array_push($fields, $panelPermissionsUser);
                if ($show_permission_showUser)  {
                    array_push($fields, $permission_showUser);
                }
                if ($show_permission_createUser)  {
                    array_push($fields, $permission_createUser);
                }
                if ($show_permission_editUser)  {
                    array_push($fields, $permission_editUser);
                }
                if ($show_permission_deleteUser)  {
                    array_push($fields, $permission_deleteUser);
                }
                if ($show_permission_showUserAdmin)  {
                    array_push($fields, $permission_showUserAdmin);
                }
                if ($show_permission_createUserAdmin)  {
                    array_push($fields, $permission_createUserAdmin);
                }
                if ($show_permission_editUserAdmin)  {
                    array_push($fields, $permission_editUserAdmin);
                }
                if ($show_permission_deleteUserAdmin)  {
                    array_push($fields, $permission_deleteUserAdmin);
                }
                if ($show_permission_createUserSuperadmin)  {
                    array_push($fields, $permission_createUserSuperadmin);
                }
                if ($show_permission_editUserSuperadmin)  {
                    array_push($fields, $permission_editUserSuperadmin);
                }
                if ($show_permission_deleteUserSuperadmin)  {
                    array_push($fields, $permission_deleteUserSuperadmin);
                }
            }
            if ($show_permission_showRole || $show_permission_createRole || $show_permission_editRole || $show_permission_deleteRole ||
                $show_permission_showRoleSuperadmin || $show_permission_showRoleUser || $show_permission_editRoleSelf || $show_permission_deleteRoleSelf)  {
                array_push($fields, $panelPermissionsRole);
                if ($show_permission_showRole)  {
                    array_push($fields, $permission_showRole);
                }
                if ($show_permission_createRole)  {
                    array_push($fields, $permission_createRole);
                }
                if ($show_permission_editRole)  {
                    array_push($fields, $permission_editRole);
                }
                if ($show_permission_deleteRole)  {
                    array_push($fields, $permission_deleteRole);
                }
                if ($show_permission_showRoleSuperadmin)  {
                    array_push($fields, $permission_showRoleSuperadmin);
                }
                if ($show_permission_editRoleSuperadmin)  {
                    array_push($fields, $permission_editRoleSuperadmin);
                }
                if ($show_permission_showRoleUser)  {
                    array_push($fields, $permission_showRoleUser);
                }
                if ($show_permission_editRoleUser)  {
                    array_push($fields, $permission_editRoleUser);
                }
                if ($show_permission_editRoleSelf)  {
                    array_push($fields, $permission_editRoleSelf);
                }
                if ($show_permission_deleteRoleSelf)  {
                    array_push($fields, $permission_deleteRoleSelf);
                }
            }
            if ($show_permission_showConfig || $show_permission_editConfig)  {
                array_push($fields, $panelPermissionsConfig);
                if ($show_permission_showConfig)  {
                    array_push($fields, $permission_showConfig);
                }
                if ($show_permission_editConfig)  {
                    array_push($fields, $permission_editConfig);
                }
				if ($show_permission_enableLoginAndRegister)  {
					array_push($fields, $permission_enableLoginAndRegister);
				}
            }
            if ($show_permission_showTab || $show_permission_createTab || $show_permission_editTab || $show_permission_deleteTab)  {
                array_push($fields, $panelPermissionsTab);
                if ($show_permission_showTab)  {
                    array_push($fields, $permission_showTab);
                }
                if ($show_permission_createTab)  {
                    array_push($fields, $permission_createTab);
                }
                if ($show_permission_editTab)  {
                    array_push($fields, $permission_editTab);
                }
                if ($show_permission_deleteTab)  {
                    array_push($fields, $permission_deleteTab);
                }
            }
            if ($show_permission_showLink || $show_permission_createLink || $show_permission_editLink || $show_permission_deleteLink)  {
                array_push($fields, $panelPermissionsLink);
                if ($show_permission_showLink)  {
                    array_push($fields, $permission_showLink);
                }
                if ($show_permission_createLink)  {
                    array_push($fields, $permission_createLink);
                }
                if ($show_permission_editLink)  {
                    array_push($fields, $permission_editLink);
                }
                if ($show_permission_deleteLink)  {
                    array_push($fields, $permission_deleteLink);
                }
            }
        }
        
        return $fields;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $response = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        if (!$this->getUser()->hasPermission('ShowRoleSuperadmin'))  {
            $response->andWhere("entity.id NOT IN ('ROLE_ADMIN')");
        }
        if (!$this->getParameter('enabled_public') || !$this->getUser()->hasPermission('ShowRoleUser')) {
            $response->andWhere("entity.id NOT IN ('ROLE_USER')");
        }

        return $response;
    }

    public function configureActions(Actions $actions): Actions
    {
        $user = $this->getUser();
        
        if (!$user->hasPermission('CreateRole')) {
            $actions->remove(Crud::PAGE_INDEX, Action::NEW);
        }
        
        $actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) use ($user) {
            return $action->displayIf(static function ($entity) use ($user)  {
                $show_edit = $user->hasPermission('EditRole');
                $show_edit = $show_edit && ($entity->getId() != "ROLE_ADMIN" || $user->hasPermission('EditRoleSuperadmin'));
                $show_edit = $show_edit && ($entity->getId() != "ROLE_USER" || $user->hasPermission('EditRoleUser'));
                $show_edit = $show_edit && (!$user->hasRole($entity->getId()) || $user->hasPermission('EditRoleSelf'));
                return $show_edit;
            });
        });
        $actions->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) use ($user) {
            return $action->displayIf(static function ($entity) use ($user)  {
                $show_edit = $user->hasPermission('EditRole');
                $show_edit = $show_edit && ($entity->getId() != "ROLE_ADMIN" || $user->hasPermission('EditRoleSuperadmin'));
                $show_edit = $show_edit && ($entity->getId() != "ROLE_USER" || $user->hasPermission('EditRoleUser'));
                $show_edit = $show_edit && (!$user->hasRole($entity->getId()) || $user->hasPermission('EditRoleSelf'));
                return $show_edit;
            });
        });

        //Disable delete ROLE_ADMIN and ROLE_USER
        $actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) use ($user) {
            return $action->displayIf(static function ($entity) use ($user)  {
                $show_delete = $user->hasPermission('DeleteRole');
                $show_delete = $show_delete && (!$user->hasRole($entity->getId()) || $user->hasPermission('DeleteRoleSelf'));
                $show_delete = $show_delete && (!in_array($entity->getId(), array("ROLE_ADMIN", "ROLE_USER")));
                return $show_delete;
            });
        });
        $actions->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) use ($user) {
            return $action->displayIf(static function ($entity) use ($user)  {
                $show_delete = $user->hasPermission('DeleteRole');
                $show_delete = $show_delete && (!$user->hasRole($entity->getId()) || $user->hasPermission('DeleteRoleSelf'));
                $show_delete = $show_delete && (!in_array($entity->getId(), array("ROLE_ADMIN", "ROLE_USER")));
                return $show_delete;
            });
        });

        return $actions;
    }
}
