<?php

namespace App\Controller\Admin\Cruds;

use App\Controller\Admin\CustomAbstractCrudController;

use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Filter\BooleanFilter;
use App\Controller\Admin\Filters\RoleFilter;

use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;

use App\Helper\FieldGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;

use App\Entity\User;
use App\Entity\Role;

class AdminCrudController extends CustomAbstractCrudController
{
    public $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
    
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setEntityLabelInPlural($this->translator->trans('entities.admin.plural'));
        $crud->setEntityLabelInSingular($this->translator->trans('entities.admin.singular'));
        $crud->setDefaultSort(['id' => 'DESC']);

        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        $em = $this->getDoctrine()->getManager();

        $name = FieldGenerator::text('name', [
            'label' => $this->translator->trans('entities.user.fields.name'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.name')
            ]
        ]);
        $lastname = FieldGenerator::text('lastname', [
            'label' => $this->translator->trans('entities.user.fields.lastname'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.lastname')
            ]
        ]);
        $fullname = FieldGenerator::text('fullname', [
            'label' => $this->translator->trans('entities.user.fields.fullname')
        ]);
        $username = FieldGenerator::text('username', [
            'label' => $this->translator->trans('entities.user.fields.username'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.username')
            ]
        ]);
        $email = FieldGenerator::email('email', [
            'label' => $this->translator->trans('entities.user.fields.email'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.email')
            ]
        ]);
        $profileImage = FieldGenerator::media('profileImage', [
            'label' => $this->translator->trans('entities.user.fields.profileImage'),
            'translator' => $this->translator,
            'acceptedFiles' => ".png, .jpeg, .jpg"
        ]);
        $password = FieldGenerator::passwordDouble('plainPassword', [
            'first_options' => [
                'label' => $this->translator->trans('entities.user.fields.password'),
                'attr' => [
                    'placeholder' => $this->translator->trans('entities.user.fields.password'),
                    'minlength' => 6,
                ],
            ],
            'second_options' => [
                'label' => $this->translator->trans('entities.user.fields.repeatPassword'),
                'attr' => [
                    'placeholder' => $this->translator->trans('entities.user.fields.repeatPassword'),
                    'minlength' => 6,
                ],
            ]
        ]);
        $existsMoreRoles = $em->getRepository(Role::class)->createQueryBuilder('r')
            ->andWhere("r.id NOT IN ('ROLE_ADMIN', 'ROLE_USER')")
            ->getQuery()->execute();
        $entityRoles = FieldGenerator::association('entityRoles', [
            'label' => $this->translator->trans('entities.role.plural'),
            'choice_label' => 'name',
            'multiple' => true,
            'choices' => $existsMoreRoles,
            'attr' => [
                'placeholder' => $this->translator->trans('entities.role.plural')
            ]
        ])->setTemplatePath('fields\role.html.twig');
        $locale = FieldGenerator::select('locale', [
            'label' => $this->translator->trans('entities.user.fields.locale'),
            'choices' => [
                '<i class="flag-icon flag-icon-us"></i> ' . "English" => "en",
                '<i class="flag-icon flag-icon-es"></i> ' . "Spanish" => "es",
            ],
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.locale')
            ],
            'required' => false
        ])->setHelp($this->translator->trans('entities.user.fields.locale_help'));
        $enabled = FieldGenerator::checkbox('enabled', [
            'label' => $this->translator->trans('entities.user.fields.enabled')
        ])->renderAsSwitch(true);
        $panelData = FormField::addPanel($this->translator->trans('entities.user.sections.data'))->setIcon('fas fa-fw fa-user-edit');
        $panelConfig = FormField::addPanel($this->translator->trans('entities.user.sections.config'))->setIcon('fas fa-fw fa-lock');
        $panelSetPassword = FormField::addPanel($this->translator->trans('entities.user.sections.setPassword'))->setIcon('fas fa-fw fa-lock');
        $panelChangePassword = FormField::addPanel($this->translator->trans('entities.user.sections.changePassword'))->setIcon('fas fa-fw fa-lock');

        //Check panels and fields visibility
        $show_entityRoles = count($existsMoreRoles) > 0;
        $required_roles = !$this->getUser()->hasPermission('CreateUserSuperadmin');

        $fields = array();
        if ($pageName == Crud::PAGE_INDEX) {
            array_push($fields, $profileImage);
            array_push($fields, $fullname);
            if ($this->container->get('session')->get('config')->enableUsername) {
                array_push($fields, $username);
            }
            array_push($fields, $email);
            if ($show_entityRoles) {
                array_push($fields, $entityRoles);
            }
            array_push($fields, $enabled);
        } else if ($pageName == Crud::PAGE_DETAIL) {
            array_push($fields, $panelData);
            array_push($fields, $name);
            array_push($fields, $lastname);
            if ($this->container->get('session')->get('config')->enableUsername) {
                array_push($fields, $username);
            }
            array_push($fields, $email);
            array_push($fields, $profileImage);
            array_push($fields, $panelConfig);
            if ($show_entityRoles) {
                array_push($fields, $entityRoles);
            }
            array_push($fields, $locale);
            array_push($fields, $enabled);
        } else if ($pageName == Crud::PAGE_NEW) {
            array_push($fields, $panelData);
            array_push($fields, $name);
            array_push($fields, $lastname);
            if ($this->container->get('session')->get('config')->enableUsername) {
                array_push($fields, $username);
            }
            array_push($fields, $email);
            array_push($fields, $profileImage);
            array_push($fields, $panelConfig);
            if ($show_entityRoles) {
                array_push($fields, $entityRoles->setRequired($required_roles));
            }
            array_push($fields, $locale);
            array_push($fields, $enabled);
            array_push($fields, $panelSetPassword);
            array_push($fields, $password->setRequired(true));
        } else if ($pageName == Crud::PAGE_EDIT) {
            array_push($fields, $panelData);
            array_push($fields, $name);
            array_push($fields, $lastname);
            if ($this->container->get('session')->get('config')->enableUsername) {
                array_push($fields, $username);
            }
            array_push($fields, $email);
            array_push($fields, $profileImage);
            array_push($fields, $panelConfig);
            if ($show_entityRoles) {
                array_push($fields, $entityRoles->setRequired($required_roles));
            }
            array_push($fields, $locale);
            array_push($fields, $enabled);
            array_push($fields, $panelChangePassword);
            array_push($fields, $password->setRequired(false));
        }
        
        return $fields;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $response = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters)
            ->leftJoin('entity.entityRoles', 'r')
            ->andWhere("r.id = 'ROLE_ADMIN'");

        return $response;
    }

    public function createEntity(string $entityFqcn)
    {
        $admin = new User();
        $admin->setVerified(true);

        return $admin;
    }
    
    public function configureFilters(Filters $filters): Filters
    {
        $em = $this->getDoctrine()->getManager();

        $existsMoreRoles = $em->getRepository(Role::class)->createQueryBuilder('r')
            ->andWhere("r.id NOT IN ('ROLE_ADMIN', 'ROLE_USER')")
            ->getQuery()->execute();

        if (count($existsMoreRoles)) {
            $filters->add(RoleFilter::new('entityRoles', $this->translator->trans('entities.role.plural')));
        }
        $filters->add(BooleanFilter::new('enabled', $this->translator->trans('entities.user.fields.enabled')));

        return $filters;
    }

    public function configureActions(Actions $actions): Actions
    {
        $user = $this->getUser();
        
        if (!$user->hasPermission('CreateUserAdmin')) {
            $actions->remove(Crud::PAGE_INDEX, Action::NEW);
        }

        $actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) use ($user) {
            return $action->displayIf(static function ($entity) use ($user)  {
                $show_edit = $user->hasPermission('EditUserAdmin');
                $show_edit = $show_edit && (!$entity->isRole('ROLE_ADMIN') || $user->hasPermission('EditUserSuperadmin'));
                return $show_edit;
            });
        });
        $actions->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) use ($user) {
            return $action->displayIf(static function ($entity) use ($user)  {
                $show_edit = $user->hasPermission('EditUserAdmin');
                $show_edit = $show_edit && (!$entity->isRole('ROLE_ADMIN') || $user->hasPermission('EditUserSuperadmin'));
                return $show_edit;
            });
        });

        $actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) use ($user) {
            return $action->displayIf(static function ($entity) use ($user)  {
                $show_delete = $user->hasPermission('DeleteUserAdmin');
                $show_delete = $show_delete && (!$entity->isRole('ROLE_ADMIN') || $user->hasPermission('DeleteUserSuperadmin'));
                return $show_delete;
            });
        });
        $actions->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) use ($user) {
            return $action->displayIf(static function ($entity) use ($user)  {
                $show_delete = $user->hasPermission('DeleteUserAdmin');
                $show_delete = $show_delete && (!$entity->isRole('ROLE_ADMIN') || $user->hasPermission('DeleteUserSuperadmin'));
                return $show_delete;
            });
        });

        return $actions;
    }

    //
    // Encode plainPassword into password
    //

    public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createEditFormBuilder($entityDto, $formOptions, $context);
        $this->addEncodePasswordEventListener($formBuilder);
        $this->addAdminRoleEventListener($formBuilder);
        if (!$this->container->get('session')->get('config')->enableUsername) {
            $this->addUsernameEventListener($formBuilder);
        }
        $this->addLocaleEventListener($formBuilder);
        return $formBuilder;
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        $this->addEncodePasswordEventListener($formBuilder);
        $this->addAdminRoleEventListener($formBuilder);
        if (!$this->container->get('session')->get('config')->enableUsername) {
            $this->addUsernameEventListener($formBuilder);
        }
        $this->addLocaleEventListener($formBuilder);
        return $formBuilder;
    }
    
    
    private $passwordEncoder;

    /**
     * @required
     */
    public function setEncoder(UserPasswordEncoderInterface $passwordEncoder): void {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function addEncodePasswordEventListener(FormBuilderInterface $formBuilder) {
        $formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $admin = $event->getData();
            if ($admin->getPlainPassword()) {
                $admin->setPassword($this->passwordEncoder->encodePassword($admin, $admin->getPlainPassword()));
            }
        });
    }

    protected function addAdminRoleEventListener(FormBuilderInterface $formBuilder) {
        $formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $em = $this->getDoctrine()->getManager();
            $admin = $event->getData();
            $admin->addEntityRole($em->getRepository(Role::class)->find("ROLE_ADMIN"));
        });
    }

    protected function addUsernameEventListener(FormBuilderInterface $formBuilder) {
        $formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $admin = $event->getData();
            $admin->setUsername($admin->getEmail());
        });
    }

    protected function addLocaleEventListener(FormBuilderInterface $formBuilder) {
        $formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $admin = $event->getData();
            if ($admin == $this->getUser()) {
                $this->container->get('session')->set('_locale', $admin->getLocale());
            }
        });
    }
}
