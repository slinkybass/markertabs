<?php

namespace App\Controller\Admin\Cruds;

use App\Controller\Admin\CustomAbstractCrudController;

use Symfony\Contracts\Translation\TranslatorInterface;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

use App\Helper\FieldGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;

use App\Entity\Tab;
use App\Entity\User;

class TabCrudController extends CustomAbstractCrudController
{
    public $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
    
    public static function getEntityFqcn(): string
    {
        return Tab::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setEntityLabelInPlural($this->translator->trans('entities.tab.plural'));
        $crud->setEntityLabelInSingular($this->translator->trans('entities.tab.singular'));
        $crud->setDefaultSort(['user' => 'ASC', 'position' => 'ASC', 'name' => 'ASC']);

        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        $em = $this->getDoctrine()->getManager();

        $name = FieldGenerator::text('name', [
            'label' => $this->translator->trans('entities.tab.fields.name'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.tab.fields.name')
            ]
        ]);
        $position = FieldGenerator::number('position', [
            'label' => $this->translator->trans('entities.tab.fields.position'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.tab.fields.position')
            ]
        ]);
        $hidden = FieldGenerator::checkbox('hidden', [
            'label' => $this->translator->trans('entities.tab.fields.hidden')
        ])->renderAsSwitch(true);
        $users = $em->getRepository(User::class)->createQueryBuilder('u')
            ->leftJoin('u.entityRoles', 'r')
            ->andWhere("r.id = 'ROLE_USER'")
            ->getQuery()->execute();
        $user = FieldGenerator::association('user', [
            'label' => $this->translator->trans('entities.user.singular'),
            'choices' => $users,
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.singular')
            ]
        ]);
        
        $panelData = FormField::addPanel($this->translator->trans('entities.tab.sections.data'))->setIcon('fas fa-fw fa-cogs');

        $fields = array();
        if ($pageName == Crud::PAGE_INDEX) {
            array_push($fields, $user);
            array_push($fields, $name);
            array_push($fields, $position);
            array_push($fields, $hidden);
        } else if ($pageName == Crud::PAGE_DETAIL) {
            array_push($fields, $panelData);
            array_push($fields, $user);
            array_push($fields, $name);
            array_push($fields, $position);
            array_push($fields, $hidden);
        } else if ($pageName == Crud::PAGE_NEW) {
            array_push($fields, $panelData);
            array_push($fields, $user);
            array_push($fields, $name);
            array_push($fields, $position);
            array_push($fields, $hidden);
        } else if ($pageName == Crud::PAGE_EDIT) {
            array_push($fields, $panelData);
            array_push($fields, $user);
            array_push($fields, $name);
            array_push($fields, $position);
            array_push($fields, $hidden);
        }
        
        return $fields;
    }
    
    public function configureFilters(Filters $filters): Filters
    {
        $filters->add(EntityFilter::new('user', $this->translator->trans('entities.user.singular')));

        return $filters;
    }

    public function configureActions(Actions $actions): Actions
    {
        $user = $this->getUser();
        
        if (!$user->hasPermission('CreateTab')) {
            $actions->remove(Crud::PAGE_INDEX, Action::NEW);
        }
        
        if (!$user->hasPermission('EditTab')) {
            $actions->remove(Crud::PAGE_INDEX, Action::EDIT);
            $actions->remove(Crud::PAGE_DETAIL, Action::EDIT);
        }
        
        if (!$user->hasPermission('DeleteTab')) {
            $actions->remove(Crud::PAGE_INDEX, Action::DELETE);
            $actions->remove(Crud::PAGE_DETAIL, Action::DELETE);
        }

        return $actions;
    }
}
