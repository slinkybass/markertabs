<?php

namespace App\Controller\Admin\Cruds;

use App\Controller\Admin\CustomAbstractCrudController;

use Symfony\Contracts\Translation\TranslatorInterface;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

use App\Helper\FieldGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;

use App\Entity\Link;

class LinkCrudController extends CustomAbstractCrudController
{
    public $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
    
    public static function getEntityFqcn(): string
    {
        return Link::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->setEntityLabelInPlural($this->translator->trans('entities.link.plural'));
        $crud->setEntityLabelInSingular($this->translator->trans('entities.link.singular'));
        $crud->setDefaultSort(['tab.user' => 'ASC', 'tab' => 'ASC', 'position' => 'ASC', 'name' => 'ASC']);

        return $crud;
    }

    public function configureFields(string $pageName): iterable
    {
        $name = FieldGenerator::text('name', [
            'label' => $this->translator->trans('entities.link.fields.name'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.link.fields.name')
            ]
        ]);
        $url = FieldGenerator::text('url', [
            'label' => $this->translator->trans('entities.link.fields.url'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.link.fields.url')
            ]
        ]);
        $image = FieldGenerator::text('image', [
            'label' => $this->translator->trans('entities.link.fields.image'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.link.fields.image')
            ]
        ]);
        $color = FieldGenerator::color('color', [
            'label' => $this->translator->trans('entities.link.fields.color'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.link.fields.color')
            ]
        ]);
        $position = FieldGenerator::number('position', [
            'label' => $this->translator->trans('entities.link.fields.position'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.link.fields.position')
            ]
        ]);
        $hidden = FieldGenerator::checkbox('hidden', [
            'label' => $this->translator->trans('entities.link.fields.hidden')
        ])->renderAsSwitch(true);
        $tab = FieldGenerator::association('tab', [
            'label' => $this->translator->trans('entities.tab.singular'),
            'choice_label' => 'adminName',
            'attr' => [
                'placeholder' => $this->translator->trans('entities.tab.singular')
            ]
        ]);
        $link = FieldGenerator::field('info', [
            'label' => $this->translator->trans('entities.link.singular')
        ])->setTemplatePath('fields\link.html.twig')->addCssClass('field-link');
        
        $panelData = FormField::addPanel($this->translator->trans('entities.link.sections.data'))->setIcon('fas fa-fw fa-cogs');

        $fields = array();
        if ($pageName == Crud::PAGE_INDEX) {
            array_push($fields, $link);
            array_push($fields, $tab);
            array_push($fields, $position);
            array_push($fields, $hidden);
        } else if ($pageName == Crud::PAGE_DETAIL) {
            array_push($fields, $panelData);
            array_push($fields, $tab);
            array_push($fields, $name);
            array_push($fields, $url);
            array_push($fields, $image);
            array_push($fields, $color);
            array_push($fields, $position);
            array_push($fields, $hidden);
        } else if ($pageName == Crud::PAGE_NEW) {
            array_push($fields, $panelData);
            array_push($fields, $tab);
            array_push($fields, $name);
            array_push($fields, $url);
            array_push($fields, $image);
            array_push($fields, $color);
            array_push($fields, $position);
            array_push($fields, $hidden);
        } else if ($pageName == Crud::PAGE_EDIT) {
            array_push($fields, $panelData);
            array_push($fields, $tab);
            array_push($fields, $name);
            array_push($fields, $url);
            array_push($fields, $image);
            array_push($fields, $color);
            array_push($fields, $position);
            array_push($fields, $hidden);
        }
        
        return $fields;
    }
    
    public function configureFilters(Filters $filters): Filters
    {
        $filters->add(EntityFilter::new('tab', $this->translator->trans('entities.tab.singular')));

        return $filters;
    }

    public function configureActions(Actions $actions): Actions
    {
        $user = $this->getUser();
        
        if (!$user->hasPermission('CreateLink')) {
            $actions->remove(Crud::PAGE_INDEX, Action::NEW);
        }
        
        if (!$user->hasPermission('EditLink')) {
            $actions->remove(Crud::PAGE_INDEX, Action::EDIT);
            $actions->remove(Crud::PAGE_DETAIL, Action::EDIT);
        }
        
        if (!$user->hasPermission('DeleteLink')) {
            $actions->remove(Crud::PAGE_INDEX, Action::DELETE);
            $actions->remove(Crud::PAGE_DETAIL, Action::DELETE);
        }

        return $actions;
    }
}
