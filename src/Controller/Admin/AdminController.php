<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use Symfony\Component\Security\Core\User\UserInterface;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;

use App\Entity\User;
use App\Entity\Role;
use App\Entity\Config;
use App\Entity\Tab;

class AdminController extends AbstractDashboardController
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $redirect = $this->generateUrl('admin_home');
        return $this->redirect($redirect);
    }

    public function configureActions(): Actions
    {
        $actions = Actions::new();

        $actions->add(Crud::PAGE_INDEX, Action::NEW);
        $actions->add(Crud::PAGE_INDEX, Action::DETAIL);
        $actions->add(Crud::PAGE_INDEX, Action::EDIT);
        $actions->add(Crud::PAGE_INDEX, Action::DELETE);
        $actions->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {return $action->setIcon('fas fa-fw fa-plus-square')->addCssClass('btn-sm');});
        $actions->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {return $action->setIcon('fas fa-fw fa-eye');});
        $actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {return $action->setIcon('fas fa-fw fa-pen');});
        $actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {return $action->setIcon('fas fa-fw fa-trash text-danger')->setCssClass('text-danger action-delete');});

        $actions->add(Crud::PAGE_NEW, Action::INDEX);
        $actions->add(Crud::PAGE_NEW, Action::SAVE_AND_RETURN);
        $actions->update(Crud::PAGE_NEW, Action::INDEX, function (Action $action) {return $action->setIcon('fas fa-fw fa-arrow-circle-left')->addCssClass('btn-sm');});
        $actions->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {return $action->setIcon('fas fa-fw fa-save')->setCssClass('btn btn-sm btn-success action-save btn-loader');});

        $actions->add(Crud::PAGE_DETAIL, Action::INDEX);
        $actions->add(Crud::PAGE_DETAIL, Action::DELETE);
        $actions->add(Crud::PAGE_DETAIL, Action::EDIT);
        $actions->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {return $action->setIcon('fas fa-fw fa-arrow-circle-left')->addCssClass('btn-sm');});
        $actions->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {return $action->setIcon('fas fa-fw fa-trash')->setCssClass('btn btn-sm btn-danger pr-0 action-delete');});
        $actions->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {return $action->setIcon('fas fa-fw fa-pen')->addCssClass('btn-sm');});

        $actions->add(Crud::PAGE_EDIT, Action::INDEX);
        $actions->add(Crud::PAGE_EDIT, Action::DELETE);
        $actions->add(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN);
        $actions->update(Crud::PAGE_EDIT, Action::INDEX, function (Action $action) {return $action->setIcon('fas fa-fw fa-arrow-circle-left')->addCssClass('btn-sm');});
        $actions->update(Crud::PAGE_EDIT, Action::DELETE, function (Action $action) {return $action->setIcon('fas fa-fw fa-trash')->setCssClass('btn btn-sm btn-danger pr-0 action-delete');});
        $actions->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {return $action->setIcon('fas fa-fw fa-save')->setCssClass('btn btn-sm btn-success action-save btn-loader');});
            
        $actions->reorder(Crud::PAGE_INDEX, [Action::DETAIL, Action::EDIT, Action::DELETE]);
        $actions->reorder(Crud::PAGE_NEW, [Action::INDEX, Action::SAVE_AND_RETURN]);
        $actions->reorder(Crud::PAGE_DETAIL, [Action::INDEX, Action::DELETE, Action::EDIT]);
        $actions->reorder(Crud::PAGE_EDIT, [Action::INDEX, Action::DELETE, Action::SAVE_AND_RETURN]);

        return $actions;
    }
    
    public function configureUserMenu(UserInterface $user): UserMenu
    {
        $userMenu = parent::configureUserMenu($user);

        $userID = $user->getId();
        $userProfileImage = $user->getProfileImage();
        $userFullname = $user->getFullname();

        $userMenu->setName($userFullname);
        if ($userProfileImage) { $userMenu->setAvatarUrl('/uploads/media/' . $userProfileImage->getFilename()); }

        $menuItems = array();
        $editUserAdminItem = MenuItem::linkToCrud($this->translator->trans('general.editUser'), 'fas fa-fw fa-user-edit', User::class)
            ->setController(Cruds\AdminCrudController::class)->setAction(Crud::PAGE_EDIT)->setEntityId($userID);
        $logOutItem = MenuItem::linkToLogout($this->translator->trans('user.sign_out', [], 'EasyAdminBundle'), 'fas fa-fw fa-sign-out-alt');
        
        if ($user->hasPermission('EditUserAdmin')) {
            array_push($menuItems, $editUserAdminItem);
        }
        array_push($menuItems, $logOutItem);
        $userMenu->setMenuItems($menuItems);
        
        return $userMenu;
    }

    public function configureCrud(): Crud
    {
        $crud = Crud::new();

        $crud->setDefaultSort(['id' => 'DESC']);
        $crud->setTimezone($this->container->get('session')->get('config')->timezone);
        $crud->showEntityActionsAsDropdown($this->container->get('session')->get('config')->entityActionsAsDropdown);
        $crud->setPaginatorPageSize($this->container->get('session')->get('config')->paginatorPageSize);
        $crud->setPaginatorRangeSize(2);
        
        $crud->setPageTitle(Crud::PAGE_INDEX, $this->translator->trans('ea.titles.list'));
        $crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('ea.titles.detail'));
        $crud->setPageTitle(Crud::PAGE_NEW, $this->translator->trans('ea.titles.new'));
        $crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit'));

        $crud->setDateFormat($this->translator->trans('format.date'));
        $crud->setTimeFormat($this->translator->trans('format.time'));
        $crud->setDateTimeFormat($this->translator->trans('format.datetime'));
        $crud->setDateIntervalFormat($this->translator->trans('format.dateinterval'));
        $crud->setNumberFormat($this->translator->trans('format.number'));
        
        return $crud;
    }

    public function configureAssets(): Assets
    {
        $assets = Assets::new();

        $assets->addCssFile('build/app.css');
        $assets->addCssFile('build/admin.css');
        $assets->addJsFile('build/app.js');
        $assets->addJsFile('build/admin.js');

        return $assets;
    }

    public function configureMenuItems(): iterable
    {  
        $em = $this->getDoctrine()->getManager();

        $config = $em->getRepository(Config::class)->findOneBy(array(), array('id' => 'DESC'));

        $homeLink = MenuItem::linktoRoute($this->translator->trans('sections.admin.home.title'), 'fas fa-fw fa-home', 'admin_home');
        $tabLink = MenuItem::linkToCrud($this->translator->trans('entities.tab.plural'), 'fas fa-fw fa-border-all', Tab::class)->setController(Cruds\TabCrudController::class);
        $linkLink = MenuItem::linkToCrud($this->translator->trans('entities.link.plural'), 'fas fa-fw fa-link', Link::class)->setController(Cruds\LinkCrudController::class);
        $mediaLink = MenuItem::linktoRoute($this->translator->trans('entities.media.plural'), 'fas fa-fw fa-file', 'admin_media');
        $userLink = MenuItem::linkToCrud($this->translator->trans('entities.user.plural'), 'fas fa-fw fa-user', User::class)->setController(Cruds\UserCrudController::class);
        $adminLink = MenuItem::linkToCrud($this->translator->trans('entities.admin.plural'), 'fas fa-fw fa-user-secret', User::class)->setController(Cruds\AdminCrudController::class);
        $userAndAdminLink = MenuItem::subMenu($this->translator->trans('entities.user.plural'), 'fas fa-fw fa-users')->setSubItems([$userLink, $adminLink]);
        $roleLink = MenuItem::linkToCrud($this->translator->trans('entities.role.plural'), 'fas fa-fw fa-user-lock', Role::class)->setController(Cruds\RoleCrudController::class);
        $configLink = MenuItem::linkToCrud($this->translator->trans('entities.config.singular'), 'fas fa-fw fa-cog', Config::class)->setController(Cruds\ConfigCrudController::class);
        $configLink = $config ? $configLink->setAction(Crud::PAGE_DETAIL)->setEntityId($config->getId()) : $configLink->setAction(Crud::PAGE_NEW);
        
        //Check panels and fields visibility
        $show_tabLink = $this->getUser()->hasPermission('ShowTab');
        $show_linkLink = $this->getUser()->hasPermission('ShowLink');
        $show_mediaLink = $this->getUser()->hasPermission('ShowMedia');
        $show_userLink = $this->getParameter('enabled_public') && $this->getUser()->hasPermission('ShowUser');
        $show_adminLink = $this->getUser()->hasPermission('ShowUserAdmin');
        $show_roleLink = $this->getUser()->hasPermission('ShowRole');
        $show_configLink = $config && $this->getUser()->hasPermission('ShowConfig') || !$config && $this->getUser()->hasPermission('EditConfig');
        
        $links = array();
        array_push($links, $homeLink);
        if ($show_tabLink) {
            array_push($links, $tabLink);
        }
        if ($show_linkLink) {
            array_push($links, $linkLink);
        }
        if ($show_mediaLink) {
            array_push($links, $mediaLink);
        }
        if ($show_userLink && $show_adminLink) {
            array_push($links, $userAndAdminLink);
        } else if ($show_userLink) {
            array_push($links, $userLink);
        } else if ($show_adminLink) {
            array_push($links, $adminLink);
        }
        if ($show_roleLink) {
            array_push($links, $roleLink);
        }
        if ($show_configLink) {
            array_push($links, $configLink);
        }
        return $links;
    }

    //
    // Custom views
    //

    /**
     * @Route("/admin/home", name="admin_home")
     */
    public function admin_home(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $admins = $em->getRepository(User::class)->createQueryBuilder('u')
            ->leftJoin('u.entityRoles', 'r')
            ->andWhere("r.id = 'ROLE_ADMIN'")
            ->orderBy("u.id", "DESC")
            ->getQuery()->execute();

        $users = $em->getRepository(User::class)->createQueryBuilder('u')
            ->leftJoin('u.entityRoles', 'r')
            ->andWhere("r.id = 'ROLE_USER'")
            ->orderBy("u.id", "DESC")
            ->getQuery()->execute();

        return $this->render('admin/home.html.twig', [
            'admins' => $admins,
            'users' => $users
        ]);
    }

    /**
     * @Route("/admin/media", name="admin_media")
     */
    public function admin_media(Request $request): Response
    {
        return $this->render('admin/media.html.twig', [
            'permission_editMedia' => $this->getUser()->hasPermission('EditMedia'),
            'permission_deleteMedia' => $this->getUser()->hasPermission('DeleteMedia'),
        ]);
    }
}
