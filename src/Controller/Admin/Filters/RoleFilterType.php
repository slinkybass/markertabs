<?php

namespace App\Controller\Admin\Filters;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Role;

class RoleFilterType extends AbstractType
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $roles = $this->em->getRepository(Role::class)->createQueryBuilder('r')
            ->andWhere("r.id NOT IN ('ROLE_USER')")
            ->getQuery()->execute();

        $choices = array();
        foreach ($roles as $role) {
            $choices[$role->getName()] = $role->getId();
        }

        $resolver->setDefaults([
            'choices' => $choices
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}