<?php

namespace App\Controller\Admin\Filters;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterInterface;
use EasyCorp\Bundle\EasyAdminBundle\Filter\FilterTrait;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDataDto;

class RoleFilter implements FilterInterface
{
    use FilterTrait;

    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setFilterFqcn(__CLASS__)
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setFormType(RoleFilterType::class)
            ->setFormTypeOption('expanded', false)
            ->setFormTypeOption('multiple', false)
            ->setFormTypeOption('attr', array("class" => 'select2'));
    }

    public function apply(QueryBuilder $queryBuilder, FilterDataDto $filterDataDto, ?FieldDto $fieldDto, EntityDto $entityDto): void
    {
        $queryBuilder->leftJoin('entity.entityRoles', 'roles');

        if ($filterDataDto->getValue() == "ROLE_ADMIN") {
            $queryBuilder->andWhere("roles.id NOT IN ('ROLE_USER')");
            $queryBuilder->groupBy("entity.id");
            $queryBuilder->having("count(roles.id) = 1");
        } else {
            $queryBuilder->andWhere("roles.id = '" . $filterDataDto->getValue() . "'");
        }
    }
}