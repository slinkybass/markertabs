<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Filesystem\Filesystem;

use App\Entity\User;
use App\Entity\Media;
use App\Form\UserFormType;
use App\Form\UserUsernameFormType;
use App\Form\UserPassFormType;
use App\Form\UserDeleteFormType;
use App\Form\MediaFormType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PublicController extends AbstractController
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/media", name="public_media")
     */
    public function public_media(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST') && $this->isLogged()) {
            $mediaFile = $request->files->get('file');
            if ($mediaFile) {
                $originalName = pathinfo($mediaFile->getClientOriginalName(), PATHINFO_FILENAME);
                $ext = $mediaFile->guessExtension();
                $fileName = uniqid() . '.' . $ext;
                $mime = $mediaFile->getMimeType();
            }

            // NEW
            if ($mediaFile && !$request->query->get('type')) {
                $media = new Media();
                // Move the file
                try {
                    $mediaFile->move($this->getParameter('media_directory'), $fileName);
                } catch (FileException $e) {
                    return $this->json($this->translator->trans('messages.errors.media.upload'), 500);
                }

                $media->setFilename($fileName);
                $media->setExt($ext);
                $media->setMime($mime);

                $media->setName($originalName);
                $media->setDatetime(new \DateTime());

                $em->persist($media);
                $em->flush();

                return $this->json($media, 200, array(), ['groups' => 'media']);
            } else if ($this->getUser()->hasPermission('EditMedia') && $request->query->get('type') && $request->query->get('type') == "UPDATE") {
                // UPDATE
                $media = $em->getRepository(Media::class)->find($request->query->get('id'));

                if ($mediaFile) {
                    // Delete the old file
                    $filesystem = new Filesystem();
                    $filesystem->remove($this->getParameter('media_directory') . "/" . $media->getFilename());
                    // Move the new file
                    try {
                        $mediaFile->move($this->getParameter('media_directory'), $fileName);
                    } catch (FileException $e) {
                        return $this->json($this->translator->trans('messages.errors.media.upload'), 500);
                    }

                    $media->setFilename($fileName);
                    $media->setExt($ext);
                    $media->setMime($mime);
                }

                $media->setName($request->request->get('name'));
                $media->setDatetime(new \DateTime());

                $em->persist($media);
                $em->flush();

                return $this->json($media, 200, array(), ['groups' => 'media']);
            } else if ($this->getUser()->hasPermission('DeleteMedia') && $request->query->get('type') && $request->query->get('type') == "DELETE") {
                // DELETE
                $media = $em->getRepository(Media::class)->find($request->query->get('id'));
                $em->remove($media);
                $em->flush();
                return $this->json($this->translator->trans('messages.oks.media.deleted'));
            }

            return $this->json($this->translator->trans('messages.errors.media.upload'), 500);
        } else if ($request->isMethod('GET')) {
            // GET
            if ($request->query->get('id')) {
                $media = $em->getRepository(Media::class)->find($request->query->get('id'));
                return $this->json($media, 200, array(), ['groups' => 'media']);
            } else if ($request->query->get('acceptedFiles')) {
                $media = $em->getRepository(Media::class)->getByMime($request->query->get('acceptedFiles'));
                return $this->json($media, 200, array(), ['groups' => 'media']);
            } else {
                $medias = $em->getRepository(Media::class)->findBy(array(), array('datetime' => 'DESC'));
                return $this->json($medias, 200, array(), ['groups' => 'media']);
            }
        }
    }

    /**
     * @Route("/", name="public_home")
     */
    public function public_home()
    {
        if (!$this->isPublicEnabled()) {
            return $this->redirectToRoute('admin');
        }

        if (!$this->isLogged()) {
            return $this->redirectToRoute('public_login');
        }

        return $this->render('public/home.html.twig', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/profile/edit", name="public_profile_edit")
     */
    public function public_profile_edit(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if (!$this->isPublicEnabled()) {
            return $this->redirectToRoute('admin');
        } else if (!$this->isLogged()) {
            return $this->redirectToRoute('public_login');
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($this->getUser()->getId());
        $profileImage = $user->getProfileImage() ? $em->getRepository(Media::class)->find($user->getProfileImage()->getId()) : new Media();

        if ($request->getSession()->get('config')->enableUsername) {
            $form = $this->createForm(UserUsernameFormType::class, $user);
        } else {
            $form = $this->createForm(UserFormType::class, $user);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if (!$request->getSession()->get('config')->enableUsername) {
                $user->setUsername($user->getEmail());
            }

            $request->getSession()->set('_locale', $user->getLocale());

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', $this->translator->trans('messages.oks.profile_edit.saved'));
        }

        $form_profileImage = $this->createForm(MediaFormType::class, $profileImage, [
            'acceptedFiles' => ".png, .jpeg, .jpg"
        ]);
        $form_profileImage->handleRequest($request);
        if ($form_profileImage->isSubmitted() && $form_profileImage->isValid()) {
            $mediaFile = $request->files->get('media_form')['file']['file'];
            if ($mediaFile) {
                $originalName = pathinfo($mediaFile->getClientOriginalName(), PATHINFO_FILENAME);
                $ext = $mediaFile->guessExtension();
                $mime = $mediaFile->getMimeType();
    
                $profileImage->setName($originalName);
                $profileImage->setExt($ext);
                $profileImage->setMime($mime);
    
                $user->setProfileImage($profileImage);
                $em->persist($profileImage);
            } else {
                $em->remove($profileImage);
            }
            $em->flush();

            $this->addFlash('success', $this->translator->trans('messages.oks.profile_edit.saved'));
        }

        $form_pass = $this->createForm(UserPassFormType::class, $user);
        $form_pass->handleRequest($request);
        if ($form_pass->isSubmitted() && $form_pass->isValid()) {
            $user->setPassword($passwordEncoder->encodePassword($user, $form_pass->get('plainPassword')->getData()));
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', $this->translator->trans('messages.oks.profile_edit.passchanged'));
        }

        $form_delete = $this->createForm(UserDeleteFormType::class, $user);
        $form_delete->handleRequest($request);
        if ($form_delete->isSubmitted() && $form_delete->isValid()) {
            $em->remove($user);
            $em->flush();

            $this->get('security.token_storage')->setToken(null);

            $this->addFlash('success', $this->translator->trans('messages.oks.profile_edit.deleted'));
            return $this->redirectToRoute('public_login');
        }

        return $this->render('public/profile_edit.html.twig', [
            'form' => $form->createView(),
            'form_profileImage' => $form_profileImage->createView(),
            'form_pass' => $form_pass->createView(),
            'form_delete' => $form_delete->createView(),
        ]);
    }
    
    private function isPublicEnabled()
    {
        if ($this->getParameter('enabled_public') && (
            !$this->isLogged() || $this->isLogged() && $this->getUser()->hasRole('ROLE_USER')
        )) {
            return true;
        }
        return false;
    }
    
    private function isLogged()
    {
        if (!$this->getUser()) {
            return false;
        }
        return true;
    }

    //
    // Custom views
    //

    /**
     * @Route("/checkPass", name="public_checkPass")
     */
    public function public_checkPass(Request $request, \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->getUser()) {
            if ($request->request->get('password')) {
                $isValid = $passwordEncoder->isPasswordValid(
                    $this->getUser(),
                    $request->request->get('password')
                );
                if ($isValid) {
                    return $this->json($this->getUser(), 200, array(), ['groups' => 'user']);
                }
            }
        }

        $obj = new \stdClass();
        $obj->message = $this->translator->trans('messages.errors.home.badCredentials');
        return $this->json($obj, 401);
    }
}
