<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;
use App\Entity\User;
use App\Entity\Role;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Psr\Container\ContainerInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;
    private $container;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, ContainerInterface $container = null)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $output = new ConsoleOutput();

        //Roles
        $output->writeln('<fg=blue>CHECKING ROLES</>');
        $roleAdmin = $manager->getRepository(Role::class)->find("ROLE_ADMIN");
        if (!$roleAdmin) {
            $roleAdmin = new Role();
            $roleAdmin->setId("ROLE_ADMIN");
            $roleAdmin->setName("Superadministrador");
            $roleAdmin->setPermissionEnableLoginAndRegister(true);
            $roleAdmin->setPermissionShowMedia(true);
            $roleAdmin->setPermissionEditMedia(true);
            $roleAdmin->setPermissionDeleteMedia(true);
            $roleAdmin->setPermissionShowUser(true);
            $roleAdmin->setPermissionCreateUser(true);
            $roleAdmin->setPermissionEditUser(true);
            $roleAdmin->setPermissionDeleteUser(true);
            $roleAdmin->setPermissionShowUserAdmin(true);
            $roleAdmin->setPermissionCreateUserAdmin(true);
            $roleAdmin->setPermissionEditUserAdmin(true);
            $roleAdmin->setPermissionDeleteUserAdmin(true);
            $roleAdmin->setPermissionCreateUserSuperadmin(true);
            $roleAdmin->setPermissionEditUserSuperadmin(true);
            $roleAdmin->setPermissionDeleteUserSuperadmin(true);
            $roleAdmin->setPermissionShowRole(true);
            $roleAdmin->setPermissionCreateRole(true);
            $roleAdmin->setPermissionEditRole(true);
            $roleAdmin->setPermissionDeleteRole(true);
            $roleAdmin->setPermissionShowRoleSuperadmin(true);
            $roleAdmin->setPermissionEditRoleSuperadmin(true);
            $roleAdmin->setPermissionShowRoleUser(true);
            $roleAdmin->setPermissionEditRoleUser(true);
            $roleAdmin->setPermissionEditRoleSelf(true);
            $roleAdmin->setPermissionDeleteRoleSelf(true);
            $roleAdmin->setPermissionShowConfig(true);
            $roleAdmin->setPermissionEditConfig(true);
            $roleAdmin->setPermissionShowTab(true);
            $roleAdmin->setPermissionCreateTab(true);
            $roleAdmin->setPermissionEditTab(true);
            $roleAdmin->setPermissionDeleteTab(true);
            $roleAdmin->setPermissionShowLink(true);
            $roleAdmin->setPermissionCreateLink(true);
            $roleAdmin->setPermissionEditLink(true);
            $roleAdmin->setPermissionDeleteLink(true);
            $manager->persist($roleAdmin);
            $output->writeln('<bg=green;options=bold>CREATED ADMIN ROLE</>');
        }
        $roleUser = $manager->getRepository(Role::class)->find("ROLE_USER");
        if (!$roleUser) {
            $roleUser = new Role();
            $roleUser->setId("ROLE_USER");
            $roleUser->setName("Usuario");
            $manager->persist($roleUser);
            $output->writeln('<bg=green;options=bold>CREATED USER ROLE</>');
        }
        $output->writeln('<fg=green>ROLES DONE</>');

        //Users
        $output->writeln('<fg=blue>CHECKING USERS</>');
        $existsAdmin = $manager->getRepository(User::class)->createQueryBuilder('u')
            ->leftJoin('u.entityRoles', 'r')
            ->andWhere("r.id = 'ROLE_ADMIN'")
            ->getQuery()->execute();
        if (!$existsAdmin) {
            $admin = new User();
            $admin->setUsername('admin@admin.com');
            $admin->setEmail('admin@admin.com');
            $admin->setName('Admin');
            $admin->setLastname('Admin');
            $admin->addEntityRole($roleAdmin);
            $admin->setPassword($this->passwordEncoder->encodePassword($admin, 'admin'));
            $admin->setEnabled(true);
            $admin->setVerified(true);
            $manager->persist($admin);
            $output->writeln('<bg=green;options=bold>CREATED ADMIN</>');
        }
        $output->writeln('<fg=green>USERS DONE</>');


        $manager->flush();
    }
}
