<?php

namespace App\Form;

use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Helper\FieldGenerator;

use App\Entity\User;

class UserDeleteFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $submit = FieldGenerator::submit('submit', [
            'label' => '<i class="fas fa-fw fa-trash"></i> ' . $this->translator->trans('sections.profile_edit.deleteAccount'),
            'attr' => [
                'class' => 'btn btn-danger',
            ],
            'row_attr' => [
                'class' => 'mb-0 text-center'
            ],
        ], true);
        
        $builder
            ->add($submit[0], $submit[1], $submit[2])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
