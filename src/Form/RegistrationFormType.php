<?php

namespace App\Form;

use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Helper\FieldGenerator;

use App\Entity\User;

class RegistrationFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $name = FieldGenerator::text('name', [
            'label' => $this->translator->trans('entities.user.fields.name'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.name')
            ],
            'label_attr' => [
                'class' => 'sr-only'
            ],
        ], true);
        $lastname = FieldGenerator::text('lastname', [
            'label' => $this->translator->trans('entities.user.fields.lastname'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.lastname')
            ],
            'label_attr' => [
                'class' => 'sr-only'
            ],
        ], true);
        $email = FieldGenerator::email('email', [
            'label' => $this->translator->trans('entities.user.fields.email'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.email')
            ],
            'label_attr' => [
                'class' => 'sr-only'
            ],
        ], true);
        $password = FieldGenerator::passwordDouble('plainPassword', [
            'mapped' => false,
            'first_options' => [
                'label' => $this->translator->trans('entities.user.fields.password'),
                'attr' => [
                    'placeholder' => $this->translator->trans('entities.user.fields.password'),
                    'minlength' => 6,
                ],
                'label_attr' => [
                    'class' => 'sr-only'
                ],
            ],
            'second_options' => [
                'label' => $this->translator->trans('entities.user.fields.repeatPassword'),
                'attr' => [
                    'placeholder' => $this->translator->trans('entities.user.fields.repeatPassword'),
                    'minlength' => 6,
                ],
                'label_attr' => [
                    'class' => 'sr-only'
                ],
            ]
        ], true);
        $agreeTerms = FieldGenerator::checkbox('agreeTerms', [
            'mapped' => false,
            'label' => $this->translator->trans('general.agreeTerms'),
            'row_attr' => [
                'class' => 'text-start mb-3',
            ]
        ], true);
        $submit = FieldGenerator::submit('submit', [
            'label' => '<i class="fas fa-fw fa-user-plus"></i> ' . $this->translator->trans('general.register'),
            'attr' => [
                'class' => 'btn btn-outline-light btn-loader',
            ],
        ], true);
        
        $builder
            ->add($name[0], $name[1], $name[2])
            ->add($lastname[0], $lastname[1], $lastname[2])
            ->add($email[0], $email[1], $email[2])
            ->add($password[0], $password[1], $password[2])
            ->add($agreeTerms[0], $agreeTerms[1], $agreeTerms[2])
            ->add($submit[0], $submit[1], $submit[2])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
