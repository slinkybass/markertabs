<?php

namespace App\Form;

use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Helper\FieldGenerator;

use App\Entity\User;

class UserFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $name = FieldGenerator::text('name', [
            'label' => $this->translator->trans('entities.user.fields.name'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.name')
            ],
        ], true);
        $lastname = FieldGenerator::text('lastname', [
            'label' => $this->translator->trans('entities.user.fields.lastname'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.lastname')
            ],
        ], true);
        $email = FieldGenerator::email('email', [
            'label' => $this->translator->trans('entities.user.fields.email'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.email')
            ],
        ], true);
        $locale = FieldGenerator::select('locale', [
            'label' => $this->translator->trans('entities.user.fields.locale'),
            'choices' => [
                '<i class="flag-icon flag-icon-us"></i> ' . "English" => "en",
                '<i class="flag-icon flag-icon-es"></i> ' . "Spanish" => "es",
            ],
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.locale')
            ],
            'required' => false,
            'help' => $this->translator->trans('entities.user.fields.locale_help'),
        ], true);
        $submit = FieldGenerator::submit('submit', [
            'label' => '<i class="fas fa-fw fa-save"></i> ' . $this->translator->trans('action.save', [], 'EasyAdminBundle'),
            'attr' => [
                'class' => 'btn btn-success btn-loader',
            ],
            'row_attr' => [
                'class' => 'mb-0 text-center'
            ],
        ], true);

        $builder
            ->add($name[0], $name[1], $name[2])
            ->add($lastname[0], $lastname[1], $lastname[2])
            ->add($email[0], $email[1], $email[2])
            ->add($locale[0], $locale[1], $locale[2])
            ->add($submit[0], $submit[1], $submit[2])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
