<?php

namespace App\Form;

use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Helper\FieldGenerator;

use App\Entity\User;

class UserPassFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $password = FieldGenerator::passwordDouble('plainPassword', [
            'mapped' => false,
            'first_options' => [
                'label' => $this->translator->trans('entities.user.fields.password'),
                'attr' => [
                    'placeholder' => $this->translator->trans('entities.user.fields.password'),
                    'minlength' => 6,
                ],
            ],
            'second_options' => [
                'label' => $this->translator->trans('entities.user.fields.repeatPassword'),
                'attr' => [
                    'placeholder' => $this->translator->trans('entities.user.fields.repeatPassword'),
                    'minlength' => 6,
                ],
            ]
        ], true);
        $submit = FieldGenerator::submit('submit', [
            'label' => '<i class="fas fa-fw fa-save btn-loader"></i> ' . $this->translator->trans('action.save', [], 'EasyAdminBundle'),
            'attr' => [
                'class' => 'btn btn-success btn-loader',
            ],
            'row_attr' => [
                'class' => 'mb-0 text-center'
            ],
        ], true);
        
        $builder
            ->add($password[0], $password[1], $password[2])
            ->add($submit[0], $submit[1], $submit[2])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
