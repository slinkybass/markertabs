<?php

namespace App\Form;

use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Helper\FieldGenerator;

use App\Entity\Media;

class MediaFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $file = FieldGenerator::file('file', [
            'label' => $this->translator->trans('entities.media.fields.file'),
            'attr' => [
                'accept' => $options['acceptedFiles']
            ],
            'help' => !$options['required'] ? $this->translator->trans('general.emptyToDeleteFile') : null
        ], true);
        $submit = FieldGenerator::submit('submit', [
            'label' => '<i class="fas fa-fw fa-save"></i> ' . $this->translator->trans('action.save', [], 'EasyAdminBundle'),
            'attr' => [
                'class' => 'btn btn-success btn-loader',
            ],
            'row_attr' => [
                'class' => 'mb-0 text-center'
            ],
        ], true);

        $builder
            ->add($file[0], $file[1], $file[2])
            ->add($submit[0], $submit[1], $submit[2])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Media::class,
            'acceptedFiles' => "*",
            'required' => false,
        ]);
    }
}
