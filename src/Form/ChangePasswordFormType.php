<?php

namespace App\Form;

use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Helper\FieldGenerator;

class ChangePasswordFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $password = FieldGenerator::passwordDouble('plainPassword', [
            'mapped' => false,
            'first_options' => [
                'label' => $this->translator->trans('entities.user.fields.password'),
                'attr' => [
                    'placeholder' => $this->translator->trans('entities.user.fields.password'),
                    'minlength' => 6,
                ],
                'label_attr' => [
                    'class' => 'sr-only'
                ],
            ],
            'second_options' => [
                'label' => $this->translator->trans('entities.user.fields.repeatPassword'),
                'attr' => [
                    'placeholder' => $this->translator->trans('entities.user.fields.repeatPassword'),
                    'minlength' => 6,
                ],
                'label_attr' => [
                    'class' => 'sr-only'
                ],
            ]
        ], true);
        $submit = FieldGenerator::submit('submit', [
            'label' => '<i class="fas fa-fw fa-save"></i> ' . $this->translator->trans('general.resetPassword'),
            'attr' => [
                'class' => 'btn btn-outline-light btn-loader',
            ],
        ], true);

        $builder
            ->add($password[0], $password[1], $password[2])
            ->add($submit[0], $submit[1], $submit[2])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
