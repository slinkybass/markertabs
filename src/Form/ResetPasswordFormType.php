<?php

namespace App\Form;

use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Helper\FieldGenerator;

class ResetPasswordFormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $email = FieldGenerator::email('email', [
            'label' => $this->translator->trans('entities.user.fields.email'),
            'attr' => [
                'placeholder' => $this->translator->trans('entities.user.fields.email')
            ],
            'label_attr' => [
                'class' => 'sr-only'
            ],
        ], true);
        $submit = FieldGenerator::submit('submit',[
            'label' => '<i class="fas fa-fw fa-user-plus"></i> ' . $this->translator->trans('general.resetPassword'),
            'attr' => [
                'class' => 'btn btn-outline-light btn-loader',
            ],
        ], true);

        $builder
            ->add($email[0], $email[1], $email[2])
            ->add($submit[0], $submit[1], $submit[2])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
