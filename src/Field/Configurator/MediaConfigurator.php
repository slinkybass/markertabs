<?php

namespace App\Field\Configurator;

use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldConfiguratorInterface;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use App\Field\MediaField;

final class MediaConfigurator implements FieldConfiguratorInterface
{
    public function supports(FieldDto $field, EntityDto $entityDto): bool
    {
        return MediaField::class === $field->getFieldFqcn();
    }

    public function configure(FieldDto $field, EntityDto $entityDto, AdminContext $context): void
    {
        $val = gettype($field->getValue()) == "object" ? $field->getValue()->getFilename() : $field->getValue();
        $configuredBasePath = $field->getCustomOption(MediaField::OPTION_BASE_PATH);
        $formattedValue = $this->getMediaPath($val, $configuredBasePath);

        $field->setFormattedValue($formattedValue);

        // this check is needed to avoid displaying broken medias when media properties are optional
        if (empty($formattedValue) || $formattedValue === rtrim($configuredBasePath ?? '', '/')) {
            $field->setTemplateName('label/empty');
        }
    }

    private function getMediaPath(?string $mediaPath, ?string $basePath): ?string
    {
        // add the base path only to medias that are not absolute URLs (http or https) or protocol-relative URLs (//)
        if (null === $mediaPath || 0 !== preg_match('/^(http[s]?|\/\/)/i', $mediaPath)) {
            return $mediaPath;
        }

        return isset($basePath)
            ? rtrim($basePath, '/').'/'.ltrim($mediaPath, '/')
            : '/'.ltrim($mediaPath, '/');
    }
}
