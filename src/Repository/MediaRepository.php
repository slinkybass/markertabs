<?php

namespace App\Repository;

use App\Entity\Media;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Media|null find($id, $lockMode = null, $lockVersion = null)
 * @method Media|null findOneBy(array $criteria, array $orderBy = null)
 * @method Media[]    findAll()
 * @method Media[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MediaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Media::class);
    }

    /**
    * @param string A comma separated of mime types or file extensions. No matter if contains spaces or spaces.
    * @param bool If false, the function dont execute ->getQuery()->getResult() .
    * @return Media[] Returns an array of Media objects filtered by MIME type.
    */
    public function getByMime($mimeTypes, $getResult = true)
    {
        $mimeTypes = explode(",", $mimeTypes);
        $mimeTypes = str_replace(array(".", " "), "", $mimeTypes);

        $query = $this->createQueryBuilder('m')
        ->where('m.mime IN (:mimeTypes)')
        ->orWhere('m.ext IN (:mimeTypes)')
        ->setParameter('mimeTypes', $mimeTypes)
        ->orderBy('m.datetime', 'DESC');

        if ($getResult) {
            return $query->getQuery()->getResult();
        } else {
            return $query;
        }
    }
}
