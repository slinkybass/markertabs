<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Entity\Config;

class ConfigSuscriber implements EventSubscriberInterface
{
    private $em;

    public function __construct(ContainerInterface $container)
    {
        $this->em = $container->get('doctrine')->getManager();
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        $session = $request->getSession();

        //Get config
        $dcConfig = $this->em->getRepository(Config::class)->findOneBy(array(), array('id' => 'DESC'));
        $dcConfig = $dcConfig ? $dcConfig : new Config();
        $config = new \stdClass();

        //Set variables
        $config->appName = $dcConfig->getAppName() ? $dcConfig->getAppName() : 'Symfony Base';
        $config->appLogo = $dcConfig->getAppLogo() ? '/uploads/media/' . $dcConfig->getAppLogo()->getFilename() : '/build/images/logo.png';
        $config->appHeaderLogo = $dcConfig->getAppHeaderLogo() ? '/uploads/media/' . $dcConfig->getAppHeaderLogo()->getFilename() : $config->appLogo;
        $config->appFavicon = $dcConfig->getAppFavicon() ? '/uploads/media/' . $dcConfig->getAppFavicon()->getFilename() : '/build/images/favicon.png';
        $config->floatingAlerts = !is_null($dcConfig->getFloatingAlerts()) ? $dcConfig->getFloatingAlerts() : true;
        $config->enableUsername = !is_null($dcConfig->getEnableUsername()) ? $dcConfig->getEnableUsername() : false;
        $config->enablePublicLogin = !is_null($dcConfig->getEnablePublicLogin()) ? $dcConfig->getEnablePublicLogin() : false;
        $config->enableRegister = !is_null($dcConfig->getEnableRegister()) ? $dcConfig->getEnableRegister() : false;
        $config->enablePublicHeader = !is_null($dcConfig->getEnablePublicHeader()) ? $dcConfig->getEnablePublicHeader() : true;
        $config->enablePublicFooter = !is_null($dcConfig->getEnablePublicFooter()) ? $dcConfig->getEnablePublicFooter() : true;
        $config->senderEmail = $dcConfig->getSenderEmail() ? $dcConfig->getSenderEmail() : 'contacto@contacto.com';
        $config->locale = $session->get('_locale') ? $session->get('_locale') : ($dcConfig->getLocale() ? $dcConfig->getLocale() : 'es');
        $config->timezone = $dcConfig->getTimezone() ? $dcConfig->getTimezone() : 'Europe/Madrid';
        $config->entityActionsAsDropdown = !is_null($dcConfig->getEntityActionsAsDropdown()) ? $dcConfig->getEntityActionsAsDropdown() : true;
        $config->paginatorPageSize = $dcConfig->getPaginatorPageSize() ? $dcConfig->getPaginatorPageSize() : 10;

        //Set locale
        $request->setLocale($config->locale);

        //Return config variable
        $session->set('config', $config);
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 20]],
        ];
    }
}