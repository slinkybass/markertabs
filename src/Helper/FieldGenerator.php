<?php

namespace App\Helper;

use Symfony\Component\Form\AbstractType;

use Doctrine\ORM\EntityRepository;

use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\HiddenField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\SlugType;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\CodeEditorType;
use Vich\UploaderBundle\Form\Type\VichFileType;

use App\Field\RangeField;
use App\Field\FileField;
use App\Field\MediaField;
use App\Entity\Media;

class FieldGenerator extends AbstractType
{
    public static function id(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "text");
        if (!$formBuilder) {
            return IdField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, TextType::class, $options);
        }
    }

    public static function text(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "text");
        if (!$formBuilder) {
            return TextField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, TextType::class, $options);
        }
    }

    public static function number(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "number");
        if (!$formBuilder) {
            return IntegerField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, IntegerType::class, $options);
        }
    }

    public static function email(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return EmailField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, EmailType::class, $options);
        }
    }

    public static function select(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "select");
        if (!$formBuilder) {
            return ChoiceField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12')
            ->setChoices($options['choices'])
            ->renderExpanded($options['expanded'])
            ->allowMultipleChoices($options['multiple']);
        } else {
            return array($name, ChoiceType::class, $options);
        }
    }

    public static function checkbox(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return BooleanField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, CheckboxType::class, $options);
        }
    }

    public static function date(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "date");
        if (!$formBuilder) {
            return DateField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12')
            ->setTimezone(date_default_timezone_get());
        } else {
            return array($name, DateType::class, $options);
        }
    }

    public static function datetime(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "datetime");
        if (!$formBuilder) {
            return DateTimeField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12')
            ->setTimezone(date_default_timezone_get());
        } else {
            return array($name, DateTimeType::class, $options);
        }
    }

    public static function time(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "time");
        if (!$formBuilder) {
            return DateTimeField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12')
            ->setTimezone(date_default_timezone_get());
        } else {
            return array($name, DateTimeType::class, $options);
        }
    }

    public static function textarea(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return TextareaField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, TextareaType::class, $options);
        }
    }

    public static function slug(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return SlugField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12')
            ->setTargetFieldName($options['target']);
        } else {
            return array($name, SlugType::class, $options);
        }
    }

    public static function hidden(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return HiddenField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, HiddenType::class, $options);
        }
    }

    public static function code(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return CodeEditorField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, CodeEditorType::class, $options);
        }
    }

    public static function array(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return ArrayField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, CollectionType::class, $options);
        }
    }

    public static function collection(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return CollectionField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, CollectionType::class, $options);
        }
    }

    public static function association(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "association");
        if (!$formBuilder) {
            return AssociationField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, EntityType::class, $options);
        }
    }

    public static function field(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return Field::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, AbstractType::class, $options);
        }
    }

    // CUSTOM FIELDS

    public static function password(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);

        if (!$formBuilder) {
            return Field::new($name)->setFormType(PasswordType::class)
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, PasswordType::class, $options);
        }
    }

    public static function passwordDouble(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        $options['type'] = PasswordType::class;

        if (!$formBuilder) {
            return Field::new($name)->setFormType(RepeatedType::class)
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, RepeatedType::class, $options);
        }
    }

    public static function texteditor(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "texteditor");
        if (!$formBuilder) {
            return TextareaField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12')
            ->setTemplatePath('fields\texteditor.html.twig');
        } else {
            return array($name, TextareaType::class, $options);
        }
    }

    public static function color(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "color");
        if (!$formBuilder) {
            return TextField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12')
            ->setTemplatePath('fields\color.html.twig');
        } else {
            return array($name, TextType::class, $options);
        }
    }

    public static function rating(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "rating");
        if (!$formBuilder) {
            return ChoiceField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12')
            ->setChoices($options['choices'])
            ->setTemplatePath('fields\rating.html.twig');
        } else {
            return array($name, ChoiceType::class, $options);
        }
    }

    public static function range(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return RangeField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, RangeType::class, $options);
        }
    }

    public static function file(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "file");
        if (!$formBuilder) {
            return FileField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, VichFileType::class, $options);
        }
    }

    public static function media(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options, "media");
        if (!$formBuilder) {
            return MediaField::new($name, $options['label'])
            ->setFormTypeOptions($options)
            ->setColumns('col-md-12');
        } else {
            return array($name, EntityType::class, $options);
        }
    }

    public static function submit(string $name, array $options = array(), bool $formBuilder = false) {
        $options = FieldGenerator::options($options);
        if (!$formBuilder) {
            return null;
        } else {
            return array($name, SubmitType::class, $options);
        }
    }

    private static function options(array $options, string $type = null) {
        // Set default values
        $options['attr'] = array_key_exists('attr', $options) ? $options['attr'] : array();
        $options['attr']['class'] = array_key_exists('class', $options['attr']) ? $options['attr']['class'] : null;
        $options['label'] = array_key_exists('label', $options) ? $options['label'] : null;
        $options['label_html'] = array_key_exists('label_html', $options) ? $options['label_html'] : true;
        
        if ($type == "text") {
            if (array_key_exists('mask', $options)) {
                $options['attr']['data-mask'] = $options['mask'];
                unset($options['mask']);
            }
        } else if ($type == "number") {
            $options['simple'] = array_key_exists('simple', $options) ? $options['simple'] : true;
            if (!$options['simple']) {
                $options['attr']['class'] .= ' input-spinner';
                if (array_key_exists('step', $options['attr']) && count(explode(".", $options['attr']['step'])) == 2) {
                    $options['attr']['data-decimals'] = strlen(explode(".", $options['attr']['step'])[1]);
                }
            }
            unset($options['simple']);
        } else if ($type == "select") {
            $options['required'] = array_key_exists('required', $options) ? $options['required'] : false;
            $options['expanded'] = array_key_exists('expanded', $options) ? $options['expanded'] : false;
            $options['multiple'] = array_key_exists('multiple', $options) ? $options['multiple'] : false;
            $options['choices'] = array_key_exists('choices', $options) ? $options['choices'] : array();

            $options['attr']['class'] .= !$options['expanded'] ? ' select2' : null;
        } else if ($type == "date") {
            $options['attr']['data-mode'] = array_key_exists('data-mode', $options['attr']) ? $options['attr']['data-mode'] : 'single';
            $options['attr']['class'] .= ' date-picker';
        } else if ($type == "datetime") {
            $options['attr']['data-mode'] = array_key_exists('data-mode', $options['attr']) ? $options['attr']['data-mode'] : 'single';
            $options['attr']['data-enable-time'] = 'true';
            $options['attr']['class'] .= ' date-picker';
        } else if ($type == "time") {
            $options['attr']['data-mode'] = array_key_exists('data-mode', $options['attr']) ? $options['attr']['data-mode'] : 'single';
            $options['attr']['data-enable-time'] = 'true';
            $options['attr']['data-no-calendar'] = 'true';
            $options['attr']['class'] .= ' date-picker';
        } else if ($type == "texteditor") {
            $options['attr']['class'] .= ' text-editor';
            if (array_key_exists('translator', $options)) {
                $options['attr']['data-locale'] = $options['translator']->trans('lang.complex');
                unset($options['translator']);
            }
        } else if ($type == "color") {
            $options['attr']['class'] .= ' color-picker';
        } else if ($type == "rating") {
            $options['attr']['class'] .= array_key_exists('mode', $options) ? ' rating-' . $options['mode'] : ' rating-star';
            if (array_key_exists('mode', $options)) {
                unset($options['mode']);
            }
        } else if ($type == "association") {
            $options['required'] = array_key_exists('required', $options) ? $options['required'] : false;
            $options['expanded'] = array_key_exists('expanded', $options) ? $options['expanded'] : false;
            $options['multiple'] = array_key_exists('multiple', $options) ? $options['multiple'] : false;

            $options['attr']['class'] .= !$options['expanded'] ? ' select2' : null;
        } else if ($type == "file") {
            $options['allow_delete'] = array_key_exists('allow_delete', $options) ? $options['allow_delete'] : false;
        } else if ($type == "media") {
            $options['class'] = array_key_exists('class', $options) ? $options['class'] : Media::class;
            $options['attr']['class'] .= $options['attr']['class'] ? ' ' : null;
            $options['attr']['class'] .= 'mediafield';
            if (array_key_exists('acceptedFiles', $options)) {
                $options['attr']['acceptedFiles'] = $options['acceptedFiles'];
                $acceptedFiles = $options['acceptedFiles'];
                $options['query_builder'] = function (EntityRepository $er) use($acceptedFiles) {
                    return $er->getByMime($acceptedFiles, false);
                };
                unset($options['acceptedFiles']);
            }
            if (array_key_exists('translator', $options)) {
                $options['attr']['dictDefaultMessage'] = $options['translator']->trans('dropzone.dictDefaultMessage');
                $options['attr']['dictFallbackMessage'] = $options['translator']->trans('dropzone.dictFallbackMessage');
                $options['attr']['dictFallbackText'] = $options['translator']->trans('dropzone.dictFallbackText');
                $options['attr']['dictFileTooBig'] = $options['translator']->trans('dropzone.dictFileTooBig');
                $options['attr']['dictInvalidFileType'] = $options['translator']->trans('dropzone.dictInvalidFileType');
                $options['attr']['dictResponseError'] = $options['translator']->trans('dropzone.dictResponseError');
                $options['attr']['dictCancelUpload'] = $options['translator']->trans('dropzone.dictCancelUpload');
                $options['attr']['dictUploadCanceled'] = $options['translator']->trans('dropzone.dictUploadCanceled');
                $options['attr']['dictCancelUploadConfirmation'] = $options['translator']->trans('dropzone.dictCancelUploadConfirmation');
                $options['attr']['dictRemoveFile'] = $options['translator']->trans('dropzone.dictRemoveFile');
                $options['attr']['dictRemoveFileConfirmation'] = $options['translator']->trans('dropzone.dictRemoveFileConfirmation');
                $options['attr']['dictMaxFilesExceeded'] = $options['translator']->trans('dropzone.dictMaxFilesExceeded');
                $options['attr']['dictFileSizeUnits'] = $options['translator']->trans('dropzone.dictFileSizeUnits');
                unset($options['translator']);
            }
        }

        return $options;
    }
}